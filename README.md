# r4d-drupal9-site
## Installation
Make sure you have the following in your settings.local.php
 ```
 $host  =  "ddev-drupal7-db";
$port  =  3306;
$driver  =  "mysql";

// If DDEV_PHP_VERSION is not set but IS_DDEV_PROJECT *is*, it means we're running (drush) on the host,
// so use the host-side bind port on docker IP
if (empty(getenv('DDEV_PHP_VERSION') &&  getenv('IS_DDEV_PROJECT') ==  'true')) {
$host  =  "127.0.0.1";
$port  =  51262;
}

$databases['migrate']['default'] =  array(
'database'  =>  "db",
'username'  =>  "db",
'password'  =>  "db",
'host'  =>  $host,
'driver'  =>  $driver,
'port'  =>  $port,
'prefix'  =>  "",
);
```
Then run:

    ddev start
