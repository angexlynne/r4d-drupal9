<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'Attribute' => $vendorDir . '/symfony/polyfill-php80/Resources/stubs/Attribute.php',
    'Composer\\InstalledVersions' => $vendorDir . '/composer/InstalledVersions.php',
    'Drupal' => $baseDir . '/web/core/lib/Drupal.php',
    'DrupalFinder\\DrupalFinder' => $vendorDir . '/webflo/drupal-finder/src/DrupalFinder.php',
    'Drupal\\Component\\DependencyInjection\\Container' => $baseDir . '/web/core/lib/Drupal/Component/DependencyInjection/Container.php',
    'Drupal\\Component\\DependencyInjection\\PhpArrayContainer' => $baseDir . '/web/core/lib/Drupal/Component/DependencyInjection/PhpArrayContainer.php',
    'Drupal\\Component\\FileCache\\FileCacheFactory' => $baseDir . '/web/core/lib/Drupal/Component/FileCache/FileCacheFactory.php',
    'Drupal\\Component\\Utility\\Timer' => $baseDir . '/web/core/lib/Drupal/Component/Utility/Timer.php',
    'Drupal\\Component\\Utility\\Unicode' => $baseDir . '/web/core/lib/Drupal/Component/Utility/Unicode.php',
    'Drupal\\Core\\Cache\\Cache' => $baseDir . '/web/core/lib/Drupal/Core/Cache/Cache.php',
    'Drupal\\Core\\Cache\\CacheBackendInterface' => $baseDir . '/web/core/lib/Drupal/Core/Cache/CacheBackendInterface.php',
    'Drupal\\Core\\Cache\\CacheTagsChecksumInterface' => $baseDir . '/web/core/lib/Drupal/Core/Cache/CacheTagsChecksumInterface.php',
    'Drupal\\Core\\Cache\\CacheTagsChecksumTrait' => $baseDir . '/web/core/lib/Drupal/Core/Cache/CacheTagsChecksumTrait.php',
    'Drupal\\Core\\Cache\\CacheTagsInvalidatorInterface' => $baseDir . '/web/core/lib/Drupal/Core/Cache/CacheTagsInvalidatorInterface.php',
    'Drupal\\Core\\Cache\\DatabaseBackend' => $baseDir . '/web/core/lib/Drupal/Core/Cache/DatabaseBackend.php',
    'Drupal\\Core\\Cache\\DatabaseCacheTagsChecksum' => $baseDir . '/web/core/lib/Drupal/Core/Cache/DatabaseCacheTagsChecksum.php',
    'Drupal\\Core\\Database\\Connection' => $baseDir . '/web/core/lib/Drupal/Core/Database/Connection.php',
    'Drupal\\Core\\Database\\Database' => $baseDir . '/web/core/lib/Drupal/Core/Database/Database.php',
    'Drupal\\Core\\Database\\Statement' => $baseDir . '/web/core/lib/Drupal/Core/Database/Statement.php',
    'Drupal\\Core\\Database\\StatementInterface' => $baseDir . '/web/core/lib/Drupal/Core/Database/StatementInterface.php',
    'Drupal\\Core\\DependencyInjection\\Container' => $baseDir . '/web/core/lib/Drupal/Core/DependencyInjection/Container.php',
    'Drupal\\Core\\DrupalKernel' => $baseDir . '/web/core/lib/Drupal/Core/DrupalKernel.php',
    'Drupal\\Core\\DrupalKernelInterface' => $baseDir . '/web/core/lib/Drupal/Core/DrupalKernelInterface.php',
    'Drupal\\Core\\Http\\InputBag' => $baseDir . '/web/core/lib/Drupal/Core/Http/InputBag.php',
    'Drupal\\Core\\Installer\\InstallerRedirectTrait' => $baseDir . '/web/core/lib/Drupal/Core/Installer/InstallerRedirectTrait.php',
    'Drupal\\Core\\Site\\Settings' => $baseDir . '/web/core/lib/Drupal/Core/Site/Settings.php',
    'JsonException' => $vendorDir . '/symfony/polyfill-php73/Resources/stubs/JsonException.php',
    'Nette\\Neon\\Decoder' => $vendorDir . '/nette/neon/src/Neon/Decoder.php',
    'Nette\\Neon\\Encoder' => $vendorDir . '/nette/neon/src/Neon/Encoder.php',
    'Nette\\Neon\\Entity' => $vendorDir . '/nette/neon/src/Neon/Entity.php',
    'Nette\\Neon\\Exception' => $vendorDir . '/nette/neon/src/Neon/Exception.php',
    'Nette\\Neon\\Lexer' => $vendorDir . '/nette/neon/src/Neon/Lexer.php',
    'Nette\\Neon\\Neon' => $vendorDir . '/nette/neon/src/Neon/Neon.php',
    'Nette\\Neon\\Node' => $vendorDir . '/nette/neon/src/Neon/Node.php',
    'Nette\\Neon\\Node\\ArrayItemNode' => $vendorDir . '/nette/neon/src/Neon/Node/ArrayItemNode.php',
    'Nette\\Neon\\Node\\ArrayNode' => $vendorDir . '/nette/neon/src/Neon/Node/ArrayNode.php',
    'Nette\\Neon\\Node\\BlockArrayNode' => $vendorDir . '/nette/neon/src/Neon/Node/BlockArrayNode.php',
    'Nette\\Neon\\Node\\EntityChainNode' => $vendorDir . '/nette/neon/src/Neon/Node/EntityChainNode.php',
    'Nette\\Neon\\Node\\EntityNode' => $vendorDir . '/nette/neon/src/Neon/Node/EntityNode.php',
    'Nette\\Neon\\Node\\InlineArrayNode' => $vendorDir . '/nette/neon/src/Neon/Node/InlineArrayNode.php',
    'Nette\\Neon\\Node\\LiteralNode' => $vendorDir . '/nette/neon/src/Neon/Node/LiteralNode.php',
    'Nette\\Neon\\Node\\StringNode' => $vendorDir . '/nette/neon/src/Neon/Node/StringNode.php',
    'Nette\\Neon\\Parser' => $vendorDir . '/nette/neon/src/Neon/Parser.php',
    'Nette\\Neon\\Token' => $vendorDir . '/nette/neon/src/Neon/Token.php',
    'Nette\\Neon\\TokenStream' => $vendorDir . '/nette/neon/src/Neon/TokenStream.php',
    'Nette\\Neon\\Traverser' => $vendorDir . '/nette/neon/src/Neon/Traverser.php',
    'Normalizer' => $vendorDir . '/symfony/polyfill-intl-normalizer/Resources/stubs/Normalizer.php',
    'PEAR_Exception' => $vendorDir . '/pear/pear_exception/PEAR/Exception.php',
    'PhpToken' => $vendorDir . '/symfony/polyfill-php80/Resources/stubs/PhpToken.php',
    'Stringable' => $vendorDir . '/symfony/polyfill-php80/Resources/stubs/Stringable.php',
    'UnhandledMatchError' => $vendorDir . '/symfony/polyfill-php80/Resources/stubs/UnhandledMatchError.php',
    'ValueError' => $vendorDir . '/symfony/polyfill-php80/Resources/stubs/ValueError.php',
);
