<?php

function r4d_assets_libraries_info() {
  $libraries = array();

  $libraries['angular'] = array(
    'name' => 'Angular',
    'vendor url' => 'https://angularjs.org/',
    'download url' => 'https://github.com/angular/angular.js',
    'version callback' => 'r4d_assets_short_circuit_version',
    'files' => array(
      'js' => array(
        'angular.js',
        'angular-ui-router.min.js',
      ),
    ),
  );

  $libraries['angular-touch'] = array(
    'name' => 'Angular Touch',
    'vendor url' => 'https://angularjs.org/',
    'download url' => 'https://github.com/angular/angular.js',
    'version callback' => 'r4d_assets_short_circuit_version',
    'files' => array(
      'js' => array(
        'angular-touch.js',
      ),
    ),
  );

  $libraries['sparkline'] = array(
    'name' => 'PHCPI Sparklines',
    'vendor url' => 'http://www.velir.com/',
    'download url' => 'http://www.velir.com/',
    'version callback' => 'r4d_assets_short_circuit_version',
    'library path' => 'sites/all/libraries/phcpi/sparkLine',
    'files' => array(
      'js' => array(
        'module.js',
        'directives/sparkLine.js',
        'services/sparkLineDataService.js',
      ),
    ),
  );

  $libraries['compare'] = array(
    'name' => 'PHCPI Compare App',
    'vendor url' => 'http://www.velir.com/',
    'download url' => 'http://www.velir.com/',
    'version callback' => 'r4d_assets_short_circuit_version',
    'library path' => 'sites/all/libraries/phcpi/apps/compare',
    'files' => array(
      'js' => array(
        'app.js',
        'controllers/CompareController.js',
        'directives/quintileBarChart.js',
        'directives/indicatorHover.js',
        'services/dataService.js',
        'services/barXScale.js'
      ),
    ),
  );

  $libraries['country'] = array(
    'name' => 'PHCPI Country App',
    'vendor url' => 'http://www.velir.com/',
    'download url' => 'http://www.velir.com/',
    'version callback' => 'r4d_assets_short_circuit_version',
    'library path' => 'sites/all/libraries/phcpi/apps/country',
    'files' => array(
      'js' => array(
        'app.js',
      ),
    ),
  );

  $libraries['home'] = array(
    'name' => 'PHCPI Home App',
    'vendor url' => 'http://www.velir.com/',
    'download url' => 'http://www.velir.com/',
    'version callback' => 'r4d_assets_short_circuit_version',
    'library path' => 'sites/all/libraries/phcpi/apps/home',
    'files' => array(
      'js' => array(
        'app.js',
        'controllers/HomeController.js',
      ),
    ),
  );

  $libraries['indicator'] = array(
    'name' => 'PHCPI Indicator App',
    'vendor url' => 'http://www.velir.com/',
    'download url' => 'http://www.velir.com/',
    'version callback' => 'r4d_assets_short_circuit_version',
    'library path' => 'sites/all/libraries/phcpi/apps/indicator',
    'files' => array(
      'js' => array(
        'app.js',
        'controllers/IndicatorController.js',
        'directives/indicatorPlot.js',
        'services/barChartAxis.js',
        'services/barChartLayout.js',
        'services/valuePlotAxis.js',
        'services/dataService.js',
        'services/valuePlotLayout.js'
      ),
    ),
  );

  $libraries['indicator_library'] = array(
    'name' => 'PHCPI Indicator Library App',
    'vendor url' => 'http://www.velir.com/',
    'download url' => 'http://www.velir.com/',
    'version callback' => 'r4d_assets_short_circuit_version',
    'library path' => 'sites/all/libraries/phcpi/apps/indicator_library',
    'files' => array(
      'js' => array(
        'app.js',
        'controllers/IndicatorLibraryController.js',
        'directives/showHide.js'
      ),
    ),
  );

  $libraries['global'] = array(
    'name' => 'PHCPI Global',
    'vendor url' => 'http://www.velir.com/',
    'download url' => 'http://www.velir.com/',
    'version callback' => 'r4d_assets_short_circuit_version',
    'library path' => 'sites/all/libraries/phcpi/global',
    'files' => array(
      'js' => array(
        'mobile_legend.js',
        'tab_groups.js',
        'collapsible.js',
        'css_blendmode_check.js',
        'indicator_slider.js',
        'indicator_sticky.js',
        'nav_drawer.js',
        'tooltip.js',
        'phcpi_init.js'
      ),
    ),
  );

  $libraries['shared'] = array(
    'name' => 'PHCPI Shared',
    'vendor url' => 'http://www.velir.com/',
    'download url' => 'http://www.velir.com/',
    'version callback' => 'r4d_assets_short_circuit_version',
    'library path' => 'sites/all/libraries/phcpi/shared',
    'files' => array(
      'js' => array(
        'module.js',
        'controllers/navController.js',
        'directives/typeAhead.js',
        'directives/shareBar.js',
        'directives/pageShare.js',
        'filters/ordinal.js',
        'services/colorGenerator.js',
        'services/parameterUtil.js',
        'services/svgTextUtil.js',
        'services/d3Util.js',
        'services/vizTooltip.js',
        'services/groupDataService.js',
        'services/appUtil.js',
        'services/hoverEventHandler.js'
      ),
    ),
  );

  $libraries['vendor'] = array(
    'name' => 'PHCPI Vendor',
    'vendor url' => 'http://www.velir.com/',
    'download url' => 'http://www.velir.com/',
    'version callback' => 'r4d_assets_short_circuit_version',
    'library path' => 'sites/all/libraries/vendor',
    'files' => array(
      'js' => array(
        'handlebars-v3.0.3.js',
        'typeahead.bundle.js',
        'lodash.js',
        'es5-shim.min.js',
        'jquery.collapsible.js',
        'jquery.bxslider.min.js'
      ),
    ),
  );

  $libraries['d3'] = array(
    'name' => 'd3',
    'vendor url' => 'http://www.d3js.org/',
    'download url' => 'http://www.d3js.org/',
    'version callback' => 'r4d_assets_short_circuit_version',
    'library path' => 'sites/all/libraries/d3',
    'files' => array(
      'js' => array(
        'd3.js',
      ),
    ),
  );

  $libraries['modernizr'] = array(
    'name' => 'modernizr',
    'vendor url' => 'http://modernizr.com/',
    'download url' => 'http://modernizr.com/',
    'version callback' => 'r4d_assets_short_circuit_version',
    'library path' => 'sites/all/libraries/modernizr',
    'files' => array(
      'js' => array(
        'modernizr-2.8.3.min.js',
      ),
    ),
  );

  return $libraries;
}

function r4d_assets_short_circuit_version() {
  return TRUE;
}

/**
 * Implements hook_variable_group_info().
 * @return mixed
 */
function r4d_assets_variable_group_info() {
  $groups['r4d'] = array(
    'title' => t('R4D'),
    'description' => t('Various front end variables'),
    'access' => 'access administration pages',
  );

  return $groups;
}

/**
 * Implements hook_variable_info().
 * @param $options
 * @return mixed
 */
function r4d_assets_variable_info($options) {
  $variables['site_managed'] = array(
    'type' => 'string',
    'group' => 'r4d',
    'title' => t('Site Managed', array(), $options),
    'default' => 'Site managed by Results for Development (R4D)',
    'description' => t('The "site managed" by line in the site footer.', array(), $options),
    'required' => TRUE,
  );

  $variables['site_address_street'] = array(
    'type' => 'string',
    'group' => 'r4d',
    'title' => t('Street Address', array(), $options),
    'default' => '1111 19th Street, NW, Suite 700',
    'description' => t('The street address for R4D offices.', array(), $options),
    'required' => TRUE,
  );

  $variables['site_address_city'] = array(
    'type' => 'string',
    'group' => 'r4d',
    'title' => t('City', array(), $options),
    'default' => 'Washington, D.C., 20036',
    'description' => t('The city for R4D offices.', array(), $options),
    'required' => TRUE,
  );

  $variables['site_twitter_handle'] = array(
    'type' => 'string',
    'group' => 'r4d',
    'title' => t('Twitter Handle', array(), $options),
    'default' => 'R4Dorg',
    'description' => t('The twitter handle for R4D.', array(), $options),
    'required' => TRUE,
  );

  $variables['site_facebook_handle'] = array(
    'type' => 'string',
    'group' => 'r4d',
    'title' => t('Facebook Handle', array(), $options),
    'default' => 'PHCPI',
    'description' => t('The Facebook handle for R4D.', array(), $options),
    'required' => TRUE,
  );

  $variables['r4d_mailchimp_list_id'] = array(
    'type' => 'string',
    'group' => 'r4d',
    'title' => t('MailChimp List ID', array(), $options),
    'default' => 'c71a3ab433',
    'description' => t('The list ID to add subscribers to. You can find this value in the MailChimp admin.', array(), $options),
    'required' => TRUE,
  );

  $variables['indicator_library_url'] = array(
    'type' => 'string',
    'group' => 'r4d',
    'title' => t('Indicator Library URL', array(), $options),
    'default' => '/',
    'description' => t('The URL of the indicator library page - this is output on the homepage for "see all indicators".', array(), $options),
    'required' => TRUE,
  );

  $variables['methodology_url'] = array(
    'type' => 'string',
    'group' => 'r4d',
    'title' => t('Methodology URL', array(), $options),
    'default' => '/',
    'description' => t('The URL of the methodology page - this is link for "learn more about missing data" links on country indicators.', array(), $options),
    'required' => TRUE,
  );

  $variables['compare_url'] = array(
    'type' => 'string',
    'group' => 'r4d',
    'title' => t('Compare URL', array(), $options),
    'default' => '/',
    'description' => t('The URL of the comparison page - this is link for "Compare this country across multiple indicators" links on country pages.', array(), $options),
    'required' => TRUE,
  );

  $variables['download_all_data_url'] = array(
    'type' => 'string',
    'group' => 'r4d',
    'title' => t('Download Data URL', array(), $options),
    'default' => '',
    'description' => t('The full URL to the file that contains all of the data in the site in a zip file.', array(), $options),
    'required' => TRUE,
  );

  $variables['feedback_label'] = array(
    'type' => 'string',
    'group' => 'r4d',
    'title' => t('Feedback Label', array(), $options),
    'default' => 'Submit Your Feedback',
    'description' => t('Appears on the button that links to the feedback form.', array(), $options),
    'required' => TRUE,
  );

  $variables['feedback_url'] = array(
    'type' => 'url',
    'group' => 'r4d',
    'title' => t('Feedback Url', array(), $options),
    'default' => 'https://www.phcperformanceinitiative.org',
    'description' => t('The full URL to the feedback form.', array(), $options),
    'required' => TRUE,
  );

  $variables['wufoo_label'] = array(
    'type' => 'string',
    'group' => 'r4d',
    'title' => t('WuFoo Label', array(), $options),
    'default' => 'WuFoo Label',
    'description' => t('Appears on the button that links to the WuFoo form.', array(), $options),
    'required' => TRUE,
  );

  $variables['wufoo_url'] = array(
    'type' => 'url',
    'group' => 'r4d',
    'title' => t('WuFoo Url', array(), $options),
    'default' => 'https://www.wufoo.com',
    'description' => t('The full URL to the WuFoo form.', array(), $options),
    'required' => TRUE,
  );

  return $variables;
}
