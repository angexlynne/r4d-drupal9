// Register our custom "cross_promotion" widget plugin with CKEditor.
CKEDITOR.plugins.add('cross_promotion', {
  requires: 'widget',
  init: function(editor) {
    // Register the toolbar buttons for the CKEditor editor instance.
    editor.ui.addButton( 'cross_promotion', {
      label : 'Insert Cross Promotion',
      icon : this.path + '/icons/icon-promo.png',
      command : 'cross_promotion'
    });
    // Register the widget.
    editor.widgets.add('cross_promotion', {
      template:
        '<div class="cross-promotion">' +
          '<h4 class="cross-promotion__title">Cross Promo Title Here.</h4>' +
          '<div class="cross-promotion__content">Content should go here.</div>' +
          '<div class="cross-promotion__cta">CTA Link Here</div>' +
        '</div>',
      allowedContent: 'div(!cross-promotion)',
      editables: {
        title: {
          selector: '.cross-promotion__title',
          allowedContent: 'h4'
        },
        content: {
          selector: '.cross-promotion__content',
          allowedContent: 'p br ul ol li strong em a a[!href]'
        },
        link: {
          selector: '.cross-promotion__cta',
          allowedContent: 'a[!href, target]'
        }
      },
      upcast: function(element) {
        return element.name === 'div' && element.hasClass('cross-promotion');
      }
    });
  }
});

