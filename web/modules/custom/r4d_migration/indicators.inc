<?php

class IndicatorMigration extends R4DMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description = t('Migrate indicators from R4D Datacenter');

    $columns = array(
      0 => array('id', 'Indicator ID'),
      1 => array('name', 'Title'),
      2 => array('created', 'Created'),
      3 => array('updated', 'Changed'),
      4 => array('displayOrder', 'Display Order'),
      5 => array('dataSource', 'Data Source'),
      6 => array('isHigherBetter', 'Is Higher Better'),
      7 => array('hasBestWorst', 'Has Best/Worst'),
      8 => array('shortName', 'Short Name'),
      9 => array('breakdown', 'Breakdown'),
      10 => array('shortDescription', 'Short Description'),
      11 => array('description', 'Description'),
      12 => array('notes', 'Notes'),
      13 => array('sourceNotes', 'Source Notes'),
      14 => array('category', 'Category'),
    );

    $this->source = new MigrateSourceCSV(
      drupal_get_path('module', 'r4d_migration') . '/csv/indicators.csv',
      $columns,
      array('header_rows' => 1)
    );

    // Set up our destination - nodes of type page
    $this->destination = new MigrateDestinationNode('indicator');

    //Source and destination relation for rollbacks
    $this->map = new MigrateSQLMap(
      $this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'name');
    #$this->addFieldMapping('body', 'description');
    #$this->addFieldMapping('body:summary', 'shortDescription');
    #$this->addFieldMapping('body:format')->defaultValue('full_html');
    $this->addFieldMapping('field_source', 'dataSource');
//    $this->addFieldMapping('field_breakdown', 'breakdown');
//    $this->addFieldMapping('field_short_name', 'shortName');
//    $this->addFieldMapping('field_units', 'units');
//    $this->addFieldMapping('field_notes', 'notes');
//    $this->addFieldMapping('field_data_source_notes', 'sourceNotes');
    $this->addFieldMapping('field_indicator_id', 'id');
//    $this->addFieldMapping('field_higher_is_better', 'higherBetter');
//    $this->addFieldMapping('field_direction_clear', 'performance');
//    $this->addFieldMapping('field_composite_indicator', 'composite');
//    $this->addFieldMapping('field_r_d_indicator', 'randd');
    $this->addFieldMapping('field_category', 'tid');
    $this->addFieldMapping('field_category:source_type')->defaultValue('tid');
    $this->addFieldMapping('status')->defaultValue(1);
    $this->addFieldMapping('uid')->defaultValue(1);
    $this->addFieldMapping('created', 'createdDate');
    $this->addFieldMapping('changed', 'updatedDate');
    $this->addFieldMapping('promote')->defaultValue(0);
    $this->addFieldMapping('sticky')->defaultValue(0);
    $this->addFieldMapping('language')->defaultValue('en');

    $this->addUnmigratedSources(
      array(
        'displayOrder',
        'indicatorNumber',
        'dataSource',
        'comments',
        'dataConstruction',
        'normativeBenchmark'
      )
    );
  }

  public function prepareRow($row) {
    $row->createdDate = REQUEST_TIME;
    $row->updatedDate = REQUEST_TIME;

    if ($term = taxonomy_get_term_by_name($row->category, 'category')) {
      $row->tid = reset($term)->tid;
    }
  }
}