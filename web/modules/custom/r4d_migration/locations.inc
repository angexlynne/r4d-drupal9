<?php

class LocationMigration extends R4DMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description = t('Migrate locations from R4D Datacenter');

    $columns = array(
      0 => array('id', 'Location ID'),
      1 => array('name', 'Title'),
      2 => array('created', 'Created'),
      3 => array('updated', 'Changed'),
      4 => array('displayOrder', 'Display Order'),
      5 => array('incomeGroup', 'Income Group'),
      6 => array('tags', 'Tags'),
      7 => array('abbreviation', 'Abbreviation'),
      8 => array('fips', 'FIPS'),
      9 => array('locationType', 'Location Type'),
      10 => array('parent', 'Parent'),
    );

    $this->source = new MigrateSourceCSV(
      drupal_get_path('module', 'r4d_migration') . '/csv/locations.csv',
      $columns,
      array('header_rows' => 1)
    );

    // Set up our destination - nodes of type page
    $this->destination = new MigrateDestinationNode('country');

    //Source and destination relation for rollbacks
    $this->map = new MigrateSQLMap(
      $this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'name');
    $this->addFieldMapping('field_abbreviation', 'abbreviation');
    $this->addFieldMapping('field_fips', 'fips');
    $this->addFieldMapping('field_location_id', 'id');
    $this->addFieldMapping('field_region', 'tid');
    $this->addFieldMapping('field_region:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_income_level', 'income_tid');
    $this->addFieldMapping('field_income_level:source_type')->defaultValue('tid');
    $this->addFieldMapping('status')->defaultValue(1);
    $this->addFieldMapping('uid')->defaultValue(1);
    $this->addFieldMapping('created', 'createdDate');
    $this->addFieldMapping('changed', 'updatedDate');
    $this->addFieldMapping('promote')->defaultValue(0);
    $this->addFieldMapping('sticky')->defaultValue(0);
    $this->addFieldMapping('language')->defaultValue('en');

    $this->addUnmigratedSources(
      array(
        'displayOrder',
        'backgroundImageId',
        'locationType',
      )
    );
  }

  public function prepareRow($row) {
    if (strtolower($row->locationType) == 'region') {
      return FALSE;
    }

    $created = new DateTime($row->created);
    $row->createdDate = $created->format('U');

    $changed = new DateTime($row->changed);
    $row->updatedDate = $changed->format('U');

    if ($term = taxonomy_get_term_by_name($row->parent, 'region')) {
      $row->tid = reset($term)->tid;
    }

    if (!empty($row->incomeGroup)) {
      switch ($row->incomeGroup) {
        case 'Low-income':
          $tag = 'Low';
          break;
        case 'Lower-middle-income':
          $tag = 'Lower - Middle Income';
          break;
        case 'Upper-middle-income':
          $tag = 'Upper - Middle Income';
          break;
      }
    }

    if (isset($tag) && $term = taxonomy_get_term_by_name($tag, 'income_level')) {
      $row->income_tid = reset($term)->tid;
    }
  }
}