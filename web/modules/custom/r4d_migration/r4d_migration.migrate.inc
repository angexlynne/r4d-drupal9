<?php

/**
 * @file
 * Because the name of this file is the module name plus '.migrate.inc', when
 * hook_migrate_api is invoked by the Migrate module this file is automatically
 * loaded - thus, you don't need to implement your hook in the .module file.
 */

/*
 * You must implement hook_migrate_api(), setting the API level to 2, if you are
 * implementing any migration classes. If your migration application is static -
 * that is, you know at implementation time exactly what migrations must be
 * instantiated - then you should register your migrations here. If your
 * application is more dynamic (for example, if selections in the UI determine
 * exactly what migrations need to be instantiated), then you would register
 * your migrations using registerMigration() - see migrate_example_baseball for
 * more information.
 */
function r4d_migration_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'content' => array(
        'title' => t('Datacenter Content'),
      ),
    ),

    'migrations' => array(
      'Indicators' => array(
        'class_name' => 'IndicatorMigration',
        'group_name' => 'content',
      ),
    ),
    'migrations' => array(
      'Locations' => array(
        'class_name' => 'LocationMigration',
        'group_name' => 'content',
      ),
    ),
  );

  return $api;
}
