<?php

/**
 * Form builder for the newsletter signup.
 * @param $form
 * @param $form_state
 * @return mixed
 */
function r4d_signup_mailchimp_form($form, &$form_state) {
  // this wrapper is necessary for the AJAX functionality to work correctly.
  $form['#prefix'] = '<div id="mailchimp-signup-form" class="site_footer__signup">';
  $form['#suffix'] = '</div>';

  $form['email'] = array(
    '#type' => 'textfield',
    '#attributes' => array('placeholder' => t('Your email address')),
    '#default_value' => '',
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Sign Up'),
    '#name' => 'newsletter-submit',
    '#ajax' => array(
      'callback' => 'r4d_signup_mailchimp_ajax_submit',
      'wrapper' => 'mailchimp-signup-form',
      'method' => 'replace',
      'progress' => 'none',
      'effect' => 'fade',
    ),
  );

  return $form;
}

/**
 * AJAX callback for r4d_signup_mailchimp_form() submit.
 * @param $form
 * @param $form_state
 * @return array
 */
function r4d_signup_mailchimp_ajax_submit(&$form, &$form_state) {
  $commands = array();
  drupal_validate_form('r4d_signup_mailchimp_form', $form, $form_state);

  if ($errors = form_get_errors()) {
    drupal_get_messages();
    if (!empty($errors['email'])) {
      $form['email']['#description'] = t($errors['email']);
    }

    // hacky workaround... not sure why blank email input does not trigger a returned error message
    if ($errors['email'] == '') {
      $form['email']['#description'] = t('Email address is required.');
    }

    $form_state['rebuild'] = TRUE;
    return $form;
  }

  // check if exists in MailChimp mailchimp_is_subscribed()
  $exists = mailchimp_is_subscribed(variable_get_value('r4d_mailchimp_list_id'), $form_state['values']['email'], TRUE);

  if ($exists) {
    $message = '<h3 class="site_footer__newsletter-title site_footer__newsletter-title--submit">
    <i class="icon icon_newsletter">
      <!--[if gte IE 9]><!-->
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
      	 width="512px" height="512px" viewBox="0 0 512 512" enable-background="new 0 0 512 512" xml:space="preserve">
      <path id="paper-plane-icon" d="M462,54.955L355.371,437.187l-135.92-128.842L353.388,167l-179.53,124.074L50,260.973L462,54.955z
      	 M202.992,332.528v124.517l58.738-67.927L202.992,332.528z"/>
      </svg>
      <!--<![endif]-->
    </i>' . t('You are already subscribed to our newsletter.') . '</h3>';
    $commands[] = ajax_command_replace('section.site_footer__newsletter h3', $message);
    $commands[] = ajax_command_remove('div#mailchimp-signup-form');
  } else {
    r4d_signup_mailchimp_subscribe_user($form, $form_state);
    $message = '<h3 class="site_footer__newsletter-title site_footer__newsletter-title--submit">
    <i class="icon icon_newsletter">
      <!--[if gte IE 9]><!-->
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
      	 width="512px" height="512px" viewBox="0 0 512 512" enable-background="new 0 0 512 512" xml:space="preserve">
      <path id="paper-plane-icon" d="M462,54.955L355.371,437.187l-135.92-128.842L353.388,167l-179.53,124.074L50,260.973L462,54.955z
      	 M202.992,332.528v124.517l58.738-67.927L202.992,332.528z"/>
      </svg>
      <!--<![endif]-->
    </i>' . t('Thank you! You are now subscribed to the PHC Vital Signs newsletter.') . '</h3>';
    $commands[] = ajax_command_replace('section.site_footer__newsletter h3', $message);
    $commands[] = ajax_command_remove('div#mailchimp-signup-form');
  }

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Validation handler for r4d_signup_mailchimp_form().
 * @param $form
 * @param $form_state
 */
function r4d_signup_mailchimp_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error("email", "Please enter a valid email address.");
  }

  if (!drupal_strlen($form_state['values']['email']) || $form_state['values']['email'] == '') {
    form_set_error("email", "Email address is required.");
  }
}

/**
 * Subscribe the user or defer it for processing later if there was an issue contacting MailChimp.
 * @param $form
 * @param $form_state
 * @return bool
 */
function r4d_signup_mailchimp_subscribe_user($form, &$form_state) {
  $success = mailchimp_subscribe(variable_get_value('r4d_mailchimp_list_id'), $form_state['values']['email'], NULL, FALSE, TRUE, 'html', FALSE, FALSE);

  if (!$success) {
    // there was some sort of error... MailChimp does not return what the error was to us.
    // we can add the email address to the queue for processing later, in the event this was a downtime or connectivity error

    $args = array(
      'list_id' => variable_get_value('r4d_mailchimp_list_id'),
      'email' => $form_state['values']['email'],
      'merge_vars' => NULL,
      'double_optin' => FALSE,
      'format' => 'html',
      'update_existing' => FALSE,
      'replace_interests' => FALSE,
      'confirm' => TRUE,
    );

    mailchimp_addto_queue('mailchimp_update_member_process', $args);
  }
}
