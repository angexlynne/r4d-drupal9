<div class="site-page-notes__copy site-page-notes__copy--references is-rte">
  <div class="block block--references"><?php print $block->subject; ?>
    <?php if (isset($title)) : ?>
      <h2><?php print $title; ?></h2>
    <?php endif; ?>

    <!-- react or js container here -->

    <ol>
      <li>Current PHC expenditure as % of Current Health Expenditure (CHE)</li>
      <li>Domestic general government PHC expenditure as % of domestic general
        government health expenditure&nbsp;
      </li>
      <li>Domestic general government PHC expenditure as % of current PHC
        expenditure
      </li>
      <li>The PHC Progression Model uses mixed methods to assess foundational
        capacities of PHC on a scale from 1 (low) to 4 (high).
      </li>
      <li>Because different data/indicators are used in each country, composite
        index values may not be comparable across countries. See page 2 for the
        specific indicators used in this VSP.
      </li>
      <li>The composite coverage index is a weighted score reflecting coverage of
        RMNCH interventions along the continuum of care
        (http://www.who.int/gho/health_equity/report_2015/en/). Certain countries
        have a country-specific index; please see Indicator Description Sheet for
        details.
      </li>
      <li>Deaths of children before age 5, per 1,000 live births.</li>
    </ol>
  </div>

</div>
