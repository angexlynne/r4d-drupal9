(function ($) {
  /**
   * Attaches the Case Study behavior.
   */
  Drupal.behaviors.caseStudy = {
    state: {
      selectedValues: {
        strategy: "*",
        country: "*"
      },
      filters: {
        $strategy: null,
        $country: null
      }
    },
    attach: function (context, settings) {
      var $context = $(context);
      var self = this;
      self.state.filters.$strategy = $context.find(".component--filter-by-strategy");
      self.state.filters.$country = $context.find(".component--filter-by-country");
      self.state.selectedValues.strategy = self.state.filters.$strategy.val();
      self.state.selectedValues.country = self.state.filters.$country.val();
      self.state.selectedValues.strategy = (self.state.selectedValues.strategy === "all") ? "" : self.state.selectedValues.strategy;
      self.state.selectedValues.country = (self.state.selectedValues.country === "all") ? "" : self.state.selectedValues.country;

      self.state.filters.$strategy.once("filter-by-strategy-handler").change(function () {
        self.state.selectedValues.strategy = (this.value === "all") ? "" : this.value;
        self.setItems($context);
        self.setAvailableOptions($context);
      });

      self.state.filters.$country.once("filter-by-country-handler").change(function () {
        self.state.selectedValues.country = (this.value === "all") ? "" : this.value;
        self.setItems($context);
        self.setAvailableOptions($context);
      });
    },
    setItems: function ($context) {
      var operator = (this.state.selectedValues.strategy !== "" && this.state.selectedValues.country !== "") ? "~" : "*";
      var selector = this.state.selectedValues.strategy + "-" + this.state.selectedValues.country;
      $context.find(".card--case-study").hide();
      $context.find("[data-case-study-selector" + operator + "=\"" + selector + "\"]").show();
    },
    setAvailableOptions: function ($context) {
      var selectorByStrategy = this.state.selectedValues.strategy + "-";
      var selectorByCountry = "-" + this.state.selectedValues.country;
      var availableStrategies = [];
      var availableCountries = [];
      $context.find("[data-case-study-selector*=\"" + selectorByStrategy + "\"]").each(function () {
        var case_study_selector = $(this).data("case-study-selector");
        case_study_selector = case_study_selector.split("-");
        if (case_study_selector[1] !== "" && availableCountries.indexOf(case_study_selector[1]) < 0) {
          availableCountries.push(case_study_selector[1]);
        }
      });

      $context.find("[data-case-study-selector*=\"" + selectorByCountry + "\"]").each(function () {
        var case_study_selector = $(this).data("case-study-selector");
        case_study_selector = case_study_selector.split("-");
        if (case_study_selector[0] !== "" && availableStrategies.indexOf(case_study_selector[0]) < 0) {
          availableStrategies.push(case_study_selector[0]);
        }
      });

      if (this.state.selectedValues.strategy !== "") {
        this.state.filters.$country.find("option").not("[value=\"all\"]").attr("disabled", "disabled");
        for (var c in availableCountries) {
          this.state.filters.$country.find("option[value=\"" + availableCountries[c] + "\"]").removeAttr("disabled");
        }
      } else {
        this.state.filters.$country.find("option").removeAttr("disabled");
      }

      if (this.state.selectedValues.country !== "") {
        this.state.filters.$strategy.find("option").not("[value=\"all\"]").attr("disabled", "disabled");
        for (var s in availableStrategies) {
          this.state.filters.$strategy.find("option[value=\"" + availableStrategies[s] + "\"]").removeAttr("disabled");
        }
      } else {
        this.state.filters.$strategy.find("option").removeAttr("disabled");
      }

    }
  };
})(jQuery);
