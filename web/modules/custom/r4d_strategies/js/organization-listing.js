(function ($) {
  /**
   * Attaches the Case Study behavior.
   */
  Drupal.behaviors.organizationListing = {
    attach: function (context, settings) {

      $('.open').on('click', function(){
        //$(this).siblings().removeClass('popup');
        $('.popup').fadeIn('slow');
        $('.popup-overlay').fadeIn('slow');
        $('.popup-overlay').height($(window).height());
        //return false;
      });

      $('.close').on('click', function(){
        $('.popup').fadeOut('slow');
        $('.popup-overlay').fadeOut('slow');
        return false;
      });

    }
  };

})(jQuery);

function openPopup(popup) {
  popup.classList.add('open-popup');
  var organization = popup.getElementsByClassName('popup-organization');
  organization[0].style.display = "block";
}

function closePopup(closeIcon) {
  var parentCloseIcon = closeIcon.parentNode;
  parentCloseIcon.style.display = "none";
}

console.log("from r4d strategies");
