<?php

/**
 * @file
 * Helper functions for R4D strategies module.
 */

function _r4d_get_strategies_options($plain = FALSE) {
  $options = [];
  $data = variable_get('r4d_is_navigation_panel_data', [
    'root' => MENU_ROOT_IS,
    'domains' => [],
  ]);
  $tree = _r4d_is_navigation_panel_get_tree($data['root'], TRUE);
  foreach ($data['domains'] as $domain) {
    foreach ($domain['strategies'] as $strategy) {
      $options[$domain['title']][$strategy] = $tree[$strategy];
      $subtree = _r4d_is_navigation_panel_get_tree($strategy, TRUE);
      foreach ($subtree as $i => $sub_strategy) {
        $options[$domain['title']][$i] = (!$plain ? '-- ' : '') . $sub_strategy;
      }
    }
  }
  return $options;
}
