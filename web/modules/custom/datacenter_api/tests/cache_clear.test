<?php

/**
 * Class CacheClearTest.
 */
class CacheClearTest extends DrupalWebTestCase {

  /**
   * Dummy user.
   */
  protected $author;

  /**
   * Installation profile to use.
   */
  protected $profile = 'testing';

  /**
   * Defines information about our test scenario.
   *
   * @return array
   */
  public static function getInfo() {
    return array(
      'name' => 'Cache Clear Tests',
      'description' => 'Functional tests that validate our custom node caches are cleared when they are saved.',
      'group' => 'PHCPI',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(array $modules = []) {
    $modules[] = 'system';
    $modules[] = 'datacenter_api';
    $modules[] = 'node';
    $modules[] = 'taxonomy';
    parent::setUp($modules);

    $this->createTaxonomy();

    $this->author = $this->drupalCreateUser(
      [
        'access content',
        'administer nodes',
        'administer taxonomy',
        'bypass node access',
        'access administration pages',
      ]
    );

    $this->drupalLogin($this->author);
  }

  /**
   * Tests that custom cache entries are flushed on node save.
   */
  public function testCustomNodeCaches() {
    $this->drupalCreateContentType(['type' => 'country', 'name' => 'Country']);
    $this->drupalCreateContentType(['type' => 'indicator', 'name' => 'Indicator']);
    $this->drupalCreateContentType(['type' => 'foobar', 'name' => 'Foobar']);

    $country_node = $this->drupalCreateNode([
      'type' => 'country',
      'status' => 1,
      'uid' => $this->author->uid,
    ]);

    $indicator_node = $this->drupalCreateNode([
      'type' => 'indicator',
      'status' => 1,
      'uid' => $this->author->uid,
    ]);

    $foobar_node = $this->drupalCreateNode([
      'type' => 'foobar',
      'status' => 1,
      'uid' => $this->author->uid,
    ]);

    // Create cache objects to simulate a user has visited a Country page.
    $data = ['foo' => 'bar'];

    cache_set('node:vital_sign_list:' . $country_node->nid, $data, 'cache', CACHE_TEMPORARY);
    cache_set('node:category_list:' . $country_node->nid, $data, 'cache', CACHE_TEMPORARY);
    cache_set('node:vital_sign_list:' . $foobar_node->nid, $data, 'cache', CACHE_TEMPORARY);
    cache_set('node:category_list:' . $foobar_node->nid, $data, 'cache', CACHE_TEMPORARY);

    $cache_item_1 = cache_get('node:vital_sign_list:' . $country_node->nid, 'cache');
    $cache_item_2 = cache_get('node:category_list:' . $country_node->nid, 'cache');
    $cache_item_3 = cache_get('node:vital_sign_list:' . $foobar_node->nid, 'cache');
    $cache_item_4 = cache_get('node:category_list:' . $foobar_node->nid, 'cache');

    // Smoke test to check our cache objects are available for the test.
    $this->assertTrue($cache_item_1);
    $this->assertTrue($cache_item_2);
    $this->assertTrue($cache_item_3);
    $this->assertTrue($cache_item_4);

    // Simulate an author updating a country node.
    $edit['title'] = $this->randomName(32);
    $this->drupalPost("node/$country_node->nid/edit", $edit, t('Save'));

    $cache_item_1 = cache_get('node:vital_sign_list:' . $country_node->nid, 'cache');
    $cache_item_2 = cache_get('node:category_list:' . $country_node->nid . '_category_list', 'cache');

    // The cache for the country node should be flushed.
    $this->assertFalse($cache_item_1);
    $this->assertFalse($cache_item_2);

    // Other node types should be unaffected and their cache objects should still exist.
    $this->assertTrue($cache_item_3);
    $this->assertTrue($cache_item_4);

    // Set the country cache items back.
    cache_set('node:vital_sign_list:' . $country_node->nid, $data, 'cache', CACHE_TEMPORARY);
    cache_set('node:category_list:' . $country_node->nid, $data, 'cache', CACHE_TEMPORARY);

    // Simulate an author updating an indicator node.
    $edit['title'] = $this->randomName(32);
    $this->drupalPost("node/$indicator_node->nid/edit", $edit, t('Save'));

    // The cache for the country node should be flushed.
    $this->assertFalse($cache_item_1);
    $this->assertFalse($cache_item_2);

    // Other node types should be unaffected and their cache objects should still exist.
    $this->assertTrue($cache_item_3);
    $this->assertTrue($cache_item_4);

    // Set the country cache items back.
    cache_set('node:vital_sign_list:' . $country_node->nid, $data, 'cache', CACHE_TEMPORARY);

    // Saving a term in vital stats should also flush the full cache.
    $term = taxonomy_get_term_by_name('Financing', 'vital_signs');
    $term = reset($term);

    $this->drupalPost("taxonomy/term/$term->tid/edit", [], t('Save'));

    $cache_item_1 = cache_get('node:vital_sign_list:' . $country_node->nid, 'cache');
    $this->assertFalse($cache_item_1, "There should be no cache item for node vital sign lists after saving a term in vital signs category.");

    // Set the country cache items back.
    cache_set('node:vital_sign_list:' . $country_node->nid, $data, 'cache', CACHE_TEMPORARY);

    // Delete a term.
    taxonomy_term_delete($term->tid);
    $cache_item_1 = cache_get('node:vital_sign_list:' . $country_node->nid, 'cache');
    $this->assertFalse($cache_item_1, "There should be no cache item for node vital sign lists after deleting a term in vital signs category.");

    // Set the country cache items back.
    cache_set('node:vital_sign_list:' . $country_node->nid, $data, 'cache', CACHE_TEMPORARY);

    // Add a term.
    $vid = taxonomy_vocabulary_machine_name_load('vital_signs')->vid;
    $term = new stdClass();
    $term->name = 'Capacity';
    $term->vid = $vid;
    taxonomy_term_save($term);

    $cache_item_1 = cache_get('node:vital_sign_list:' . $country_node->nid, 'cache');
    $this->assertFalse($cache_item_1, "There should be no cache item for node vital sign lists after adding a term in vital signs category.");
  }

  /**
   * Generate required taxonomy.
   */
  protected function createTaxonomy() {
    $entity = (object) array(
      'name' => 'Vital Signs',
      'description' => 'This vocabulary is for testing.',
      'machine_name' => 'vital_signs',
    );

    taxonomy_vocabulary_save($entity);

    $vid = taxonomy_vocabulary_machine_name_load('vital_signs')->vid;

    $term = new stdClass();
    $term->name = 'Financing';
    $term->vid = $vid;
    taxonomy_term_save($term);
  }
}