<?php

/**
 * Class TaxonomyDescriptionOverrideTest.
 */
class TaxonomyDescriptionOverrideTest extends DrupalWebTestCase {

  /**
   * Dummy user.
   */
  protected $author;

  /**
   * List of dummy countries.
   *
   * @var array
   */
  protected $countries = [];

  /**
   * Defines information about our test scenario.
   *
   * @return array
   */
  public static function getInfo() {
    return array(
      'name' => 'Taxonomy Description Override Tests',
      'description' => 'Functional tests that ensure that taxonomy descriptions can be overridden.',
      'group' => 'PHCPI',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(array $modules = []) {
    $modules[] = 'datacenter_api';
    $modules[] = 'entity';
    parent::setUp($modules);

    // This tests needs vocabs to exist first before enabling modules.
    $this->createVocabulary();

    // Features have to be handled differently.
    module_enable(['features', 'field_bases']);
    module_enable(['vital_signs_taxonomy', 'category_taxonomy']);
    module_enable(['country_configuration']);

    $this->setVitalSignDefaultDescription();

    $this->author = $this->drupalCreateUser(
      [
        'access content',
        'administer nodes',
        'administer taxonomy',
        'bypass node access',
        'access administration pages',
      ]
    );

    $this->drupalLogin($this->author);
  }

  /**
   * Test that we can get the term default or the overridden value from a field
   * collection for certain countries.
   */
  public function testTaxonomyDescriptionOverride() {
    $countries = [
      'China',
      'Uganda',
      'Sri Lanka'
    ];

    $country_list = [];

    foreach ($countries as $country) {
      $node = $this->drupalCreateNode([
        'type' => 'country',
        'status' => 1,
        'title' => $country,
        'uid' => $this->author->uid,
        'created' => REQUEST_TIME,
        'changed' => REQUEST_TIME,
      ]);

      $country_list[$country] = $node->nid;
    }

    // Test Financing returns the default.
    $term = $this->getTaxonomyTermByName('Financing', 'vital_signs');

    foreach ($country_list as $country => $nid) {
      $description = datacenter_api_get_taxonomy_term_description($term->tid, $nid);
      $this->assertIdentical($description, 'This is the default description.', "Asserting that {$country} gets the default term description for Financing.");
    }

    // Test Capacity returns the default.
    $term = $this->getTaxonomyTermByName('Capacity', 'vital_signs');

    foreach ($country_list as $country => $nid) {
      $description = datacenter_api_get_taxonomy_term_description($term->tid, $nid);
      $this->assertIdentical($description, 'This is the default description.', "Asserting that {$country} gets the default term description for Capacity.");
    }

    // Edit Capacity and add an override for Uganda.
    $collection = entity_create('field_collection_item', array('field_name' => 'field_description_overrride'));
    $collection->setHostEntity('taxonomy_term', $term);

    // Add an override for Uganda on Capacity.
    $wrapper = entity_metadata_wrapper('field_collection_item', $collection);
    $wrapper->field_country->set($country_list['Uganda']);
    $wrapper->field_description->set(array('value' => 'I am a custom override for Uganda.', 'format' => 'restricted_html'));
    $wrapper->save();

    // Test Capacity returns the default, but not for Uganda.
    $term = $this->getTaxonomyTermByName('Capacity', 'vital_signs');
    $description = datacenter_api_get_taxonomy_term_description($term->tid, $country_list['Uganda']);
    $this->assertIdentical($description, 'I am a custom override for Uganda.', "Asserting that Uganda gets the overridden term description for Capacity.");
    $description = datacenter_api_get_taxonomy_term_description($term->tid, $country_list['China']);
    $this->assertIdentical($description, 'This is the default description.', "Asserting that China gets the default term description for Capacity.");

    // Edit Financing and add an override for China.
    $term = $this->getTaxonomyTermByName('Financing', 'vital_signs');
    $collection = entity_create('field_collection_item', array('field_name' => 'field_description_overrride'));
    $collection->setHostEntity('taxonomy_term', $term);

    // Add an override for China on Financing.
    $wrapper = entity_metadata_wrapper('field_collection_item', $collection);
    $wrapper->field_country->set($country_list['China']);
    $wrapper->field_description->set(array('value' => 'I am a custom override for China.', 'format' => 'restricted_html'));
    $wrapper->save();

    $term = $this->getTaxonomyTermByName('Financing', 'vital_signs');
    $description = datacenter_api_get_taxonomy_term_description($term->tid, $country_list['Uganda']);
    $this->assertIdentical($description, 'This is the default description.', "Asserting that Uganda gets the default term description for Financing.");
    $description = datacenter_api_get_taxonomy_term_description($term->tid, $country_list['China']);
    $this->assertIdentical($description, 'I am a custom override for China.', "Asserting that China gets the overridden term description for Financing.");
  }

  /**
   * Generate required taxonomy.
   */
  protected function createVocabulary() {
    $vocabulary = [
      [
        'name' => 'Article Category',
        'machine_name' => 'article_category',
      ],
      [
        'name' => 'Vital Signs Category',
        'machine_name' => 'vital_signs',
      ],
      [
        'name' => 'Category Category',
        'machine_name' => 'category',
      ],
      [
        'name' => 'Improvement Type',
        'machine_name' => 'improvement_type'
      ],
      [
        'name' => 'Regions',
        'machine_name' => 'region'
      ]
    ];

    foreach ($vocabulary as $vocab) {
      $entity = (object) array(
        'name' => $vocab['name'],
        'description' => 'This vocabulary is for testing.',
        'machine_name' => $vocab['machine_name'],
      );

      taxonomy_vocabulary_save($entity);
    }
  }

  /**
   * Fetch a term by name.
   *
   * This returns the direct result (term object) to save on lines of code.
   *
   * @param string $name
   * @param string $vocab_machine_name
   * @return mixed
   */
  protected function getTaxonomyTermByName(string $name, string $vocab_machine_name) {
    $result = taxonomy_get_term_by_name($name, $vocab_machine_name);
    return reset($result);
  }

  /**
   * Set the default descriptions before tests run.
   */
  protected function setVitalSignDefaultDescription() {
    $categories = taxonomy_get_tree(taxonomy_vocabulary_machine_name_load('vital_signs')->vid);

    foreach ($categories as $key => $category) {
      $category->description = 'This is the default description.';
      taxonomy_term_save($category);
    }
  }
}
