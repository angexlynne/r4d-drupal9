<?php

declare(strict_types = 1);

class DonutChart {

  const MAXVALUE = 100;

  /**
   * @var int
   */
  protected $actual_value;

  /**
   * @var int
   */
  protected $remaining_value;

  public function __construct(int $raw_value) {
    if ($raw_value > 100) {
      throw new Exception('Raw value for a donut chart must be greater than 0, and less than 100.');
    }

    $this->actual_value = $raw_value;
    $this->remaining_value = (self::MAXVALUE - $raw_value);
  }

  /**
   * @return int
   */
  public function getActualValue() : int {
    return $this->actual_value;
  }

  /**
   * @return int
   */
  public function getRemainingValue() : int {
    return $this->remaining_value;
  }

}
