<?php

/**
 * Menu callback - return an admin settings form.
 * @param $form
 * @param $form_state
 * @return mixed
 */
function datacenter_api_settings_form($form, $form_state) {
  $form['datacenter_hostname'] = array(
    '#type' => 'textfield',
    '#description' => 'Enter the hostname (including http://) where the Datacenter data resides.',
    '#default_value' => variable_get('datacenter_hostname', ''),
  );

  $form['datacenter_webshot_url'] = array(
    '#type' => 'textfield',
    '#description' => 'Enter the URL (including http://) to the webshot image generation service.',
    '#default_value' => variable_get('datacenter_webshot_url', ''),
  );

  $form['datacenter_enable_ssl_verification'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable SSL Verification'),
    '#description' => 'Check this box to enable SSL verification, which should always be active. Disabling is useful when in lower environments that use self signed certificates.',
    '#default_value' => variable_get('datacenter_enable_ssl_verification', TRUE),
  );

  return system_settings_form($form);
}