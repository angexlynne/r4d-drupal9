<?php

interface DatacenterConnectorInterface {
  public function __construct($base_url, array $options = array());
  public function getLocation(array $arguments);
  public function getIndicator(array $arguments);
}