<?php

use GuzzleHttp\Client as GuzzleClient;

class DatacenterConnector implements DatacenterConnectorInterface {
  protected $client;

  public function __construct($base_url, array $options = array()) {
    $this->client = new GuzzleClient(['base_uri' => $base_url, 'verify' => (bool) variable_get('datacenter_enable_ssl_verification', TRUE)]);
  }

  /**
   * Returns a response with all data relevant to the requested location ID.
   * @param array $arguments
   * @return bool|mixed
   */
  public function getLocation(array $arguments) {
    return $this->request('/api/page/location', array('id' => implode(',', $arguments)));
  }

  /**
   * Returns a response with all data relevant to the requested indicator ID.
   * @param array $arguments
   * @return bool|mixed
   */
  public function getIndicator(array $arguments) {
    return $this->request('/api/page/indicator', array('id' => implode(',', $arguments)));
  }

  /**
   * Performs a request with GuzzleHttp\Client and returns the JSON response.
   * @param $endpoint
   * @param array $arguments
   * @return bool|mixed
   *
   * @throws \Exception
   */
  protected function request($endpoint, array $arguments) {
    try {
      $response = $this->client->get($endpoint, ['query' => $arguments]);
      $content = $response->getBody()->getContents();
      return json_decode($content, TRUE);
    } catch (\Exception $error) {
      watchdog('datacenter_api', 'Error retrieving data for id @id at endpoint @endpoint. Additional info: @error', array('@id' => $arguments['id'], '@endpoint' => $endpoint, '@error' => $error->getMessage()), WATCHDOG_CRITICAL);
      return FALSE;
    }
  }
}