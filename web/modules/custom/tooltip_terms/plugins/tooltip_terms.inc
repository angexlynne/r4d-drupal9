<?php

/**
 * @file
 * Wysiwyg API integration.
 */

/**
 * Implementats of hook_wysiwyg_plugin().
 */
function tooltip_terms_tooltip_terms_plugin() {
  $plugins['tooltip_terms'] = [
    'title' => t('Tooltip term'),
    'vendor url' => 'http://drupal.org/project/tooltip_terms',
    'icon file' => 'tooltip_terms.png',
    'icon title' => t('Insert a tooltip term'),
    'settings' => [],
  ];
  return $plugins;
}

