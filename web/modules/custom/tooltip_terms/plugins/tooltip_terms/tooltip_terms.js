/*jshint strict:true, browser:true, curly:true, eqeqeq:true, expr:true, forin:true, latedef:true, newcap:true, noarg:true, trailing: true, undef:true, unused:true */
/*global Drupal:true, jQuery: true*/
(function ($, Drupal) {
  "use strict";
  var dialogs = {};

  /**
   * Wysiwyg plugin button implementation for tooltip_terms plugin.
   */
  Drupal.wysiwyg.plugins.tooltip_terms = {
    /**
     * Return whether the passed node belongs to this plugin.
     *
     * @param node
     *   The currently focused DOM element in the editor content.
     */
    isNode: function (node) {
      return ($(node).is("img.tooltip_terms-tooltip_term"));
    },

    /**
     * Execute the button.
     *
     * @param data
     *   An object containing data about the current selection:
     *   - format: 'html' when the passed data is HTML content, 'text' when the
     *     passed data is plain-text content.
     *   - node: When 'format' is 'html', the focused DOM element in the editor.
     *   - content: The textual representation of the focused/selected editor
     *     content.
     * @param settings
     *   The plugin settings, as provided in the plugin's PHP include file.
     * @param instanceId
     *   The ID of the current editor instance.
     */
    invoke: function (data, settings, instanceId) {
      Drupal.wysiwyg.plugins.tooltip_terms.insert_form(data, settings, instanceId);
    },


    insert_form: function (data, settings, instanceId) {
      if (!dialogs[instanceId]) {
        var id = "tooltip-terms-insert-dialog-" + instanceId;
        var $dialogdiv = $("<div id='" + id + "'></div>").appendTo("body");
        var ajax_settings = {
          url: Drupal.settings.basePath + "tooltip_terms/insert/" + instanceId,
          event: "dialog.tooltip-terms-insert-wysiwyg",
          method: "html"
        };
        new Drupal.ajax(id, $dialogdiv[0], ajax_settings);
        $dialogdiv.trigger(ajax_settings.event);
      } else {
        dialogs[instanceId].dialog("open");
      }
    },

    insertIntoEditor: function (tooltipElement, editor_id) {
      Drupal.wysiwyg.instances[editor_id].insert(tooltipElement);
    },

    /**
     * Prepare all plain-text contents of this plugin with HTML representations.
     *
     * Optional; only required for "inline macro tag-processing" plugins.
     *
     * @param content
     *   The plain-text contents of a textarea.
     * @param settings
     *   The plugin settings, as provided in the plugin's PHP include file.
     * @param instanceId
     *   The ID of the current editor instance.
     */
    attach: function (content, settings) {
      if (document.getElementById("tooltip-terms-canvas") == null) {
        $("<canvas id='tooltip-terms-canvas' height='16px'></canvas>").appendTo("body");
      }
      var canvasEl = document.getElementById("tooltip-terms-canvas");
      var canvasCtx = canvasEl.getContext("2d");

      var matches = content.matchAll(/<!--tooltip-terms-term-(.+?)-tid-(\d+)-->/g);
      matches = Array.from(matches);

      for (var i in matches) {
        canvasCtx.font = "16px Neuton, serif";
        canvasCtx.canvas.width = canvasCtx.measureText(matches[i][1]).width;
        canvasCtx.font = "16px Neuton, serif";
        canvasCtx.fillText(matches[i][1], 0, 16);
        var src = canvasCtx.canvas.toDataURL();
        canvasCtx.clearRect(0, 0, canvasEl.width, canvasEl.height);
        var regex = new RegExp(matches[i][0], "gmi");
        content = content.replace(regex, "<img contenteditable='false' title='Tooltip Term' src='" + src + "' class='tooltip_terms-tooltip_term drupal-content' data-tooltip-terms-value='term-" + matches[i][1] + "-tid-" + matches[i][2] + "'/>");
      }

      return content;
    },

    /**
     * Process all HTML placeholders of this plugin with plain-text contents.
     *
     * Optional; only required for "inline macro tag-processing" plugins.
     *
     * @param content
     *   The HTML content string of the editor.
     * @param settings
     *   The plugin settings, as provided in the plugin's PHP include file.
     * @param instanceId
     *   The ID of the current editor instance.
     */
    detach: function (content) {
      var $content = $("<div>" + content + "</div>");
      $.each($("img.tooltip_terms-tooltip_term", $content), function (i, elem) {
        var $el = $(elem);
        var data = $el.data("tooltip-terms-value");
        var comment = "<!--tooltip-terms-" + data + "-->";
        $el.replaceWith(comment);
      });

      return $content.html();
    },

    /**
     * Helper function to return a HTML placeholder.
     *
     * The 'drupal-content' CSS class is required for HTML elements in the editor
     * content that shall not trigger any editor's native buttons (such as the
     * image button for this example placeholder markup).
     */
    _getPlaceholder: function (settings) {
      return "<img src='" + settings.path + "/images/spacer.gif' alt='&lt;--tooltip_terms-&gt;' title='&lt;--tooltip_terms--&gt;' class='tooltip_terms-tooltip_terms drupal-content' />";
    }
  };

  Drupal.ajax.prototype.commands.tooltipTermsInsertTable = function (ajax, response, status) {
    var $dialogdiv = $(response.selector);
    var instanceId = response.instance_id;
    var tableRows = [];
    $dialogdiv.once("load-tooltip-term-rows", function () {
      var $rows = $(this).find(".tooltip-term-filter");
      $rows.each(function () {
        var $row = $(this);
        var term = $row.data("term");
        $row = $row.closest("tr");
        tableRows.push({
          "term": term,
          "$row": $row
        });
      });
    });
    $dialogdiv.once("trigger-tooltip-term-insert", function () {
      $(this).find(".trigger-tooltip-term-insert").click(function () {
        var term = $(this).data("term");
        var tid = $(this).data("tid");
        var tooltipElement = "<!--tooltip-terms-term-" + term + "-tid-" + tid + "-->";
        Drupal.wysiwyg.plugins.tooltip_terms.insertIntoEditor(tooltipElement, instanceId);
      });
    });

    $dialogdiv.once("trigger-tooltip-terms-filter", function () {
      $(this).find(".tooltip-terms-filter").keyup(function () {
        var keyword = this.value.toLowerCase();
        var show = false;
        var odd = false;
        for (var i in tableRows) {
          show = ((keyword.length === 0) || (tableRows[i].term.toLowerCase().indexOf(keyword) >= 0));
          tableRows[i].$row.toggle(show);
          if (show) {
            odd = !odd;
            tableRows[i].$row.toggleClass("odd", odd).toggleClass("even", !odd);
          }
        }
      });
    })

    var btns = {};

    btns[Drupal.t("Cancel")] = function () {
      $dialogdiv.dialog("close");
    };

    $dialogdiv.dialog({
      modal: false,
      autoOpen: false,
      closeOnEscape: true,
      resizable: false,
      draggable: true,
      autoresize: true,
      namespace: "jquery_ui_dialog_default_ns_tooltip_terms",
      dialogClass: "jquery_ui_dialog-dialog dialog-tooltip-terms",
      title: Drupal.t("Insert Tooltip"),
      buttons: btns,
      width: 700
    });
    $dialogdiv.css({maxHeight: 500});
    dialogs[instanceId] = $dialogdiv;
    $dialogdiv.dialog("open");
  };
})(jQuery, Drupal);
