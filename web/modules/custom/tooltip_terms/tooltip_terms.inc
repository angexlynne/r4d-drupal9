<?php
/**
 * @file
 * Helper functions for tooltip_terms.module.
 */

function _tooltip_terms_get_terms() {
  $terms = [];
  // @FIXME
// // @FIXME
// // This looks like another module's variable. You'll need to rewrite this call
// // to ensure that it uses the correct configuration object.
// $taxonomies = variable_get('tooltip_term_taxonomies', '');

  if (!empty($taxonomies)) {
    if (is_array($taxonomies)) {
      $taxonomies = array_filter($taxonomies);
    }
    else {
      $taxonomies = [$taxonomies];
    }
    $taxonomies = taxonomy_vocabulary_load_multiple($taxonomies);
    foreach ($taxonomies as $taxonomy) {
      $terms = taxonomy_get_tree($taxonomy->vid);
    }
  }
  return $terms;
}

function _tooltip_terms_get_rows($terms) {
  $rows = [];
  foreach ($terms as $term) {
    $row = [
      '<div class="trigger-tooltip-term-insert tooltip-term-filter" data-tid="' . $term->tid . '" data-term="' . $term->name . '" data-tooltip="' . $term->description . '">' . $term->name . '</div>',
      '<div class="trigger-tooltip-term-insert" data-tid="' . $term->tid . '" data-term="' . $term->name . '" data-tooltip="' . $term->description . '">' . $term->description . '</div>',
    ];
    $rows[] = $row;
  }
  return $rows;
}
