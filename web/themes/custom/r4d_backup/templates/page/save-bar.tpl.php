<span class="share-toolbar__description"><?php print t('SAVE'); ?>:</span>

<ul class="share-toolbar__list">
<!--  <li class="share-toolbar__list-item">-->
<!--    <a data-print="pdf" class="share-toolbar__icon share-toolbar__icon--pdf shareBar" href="" data-print-url="/print/--><?php //print $node_url; ?><!--"></a>-->
<!--  </li>-->
  <li class="share-toolbar__list-item">
    <a data-print="png" target="_blank" class="share-toolbar__icon share-toolbar__icon--image shareBar" href="" data-print-url="/print/<?php print $node_url; ?>"></a>
  </li>
</ul>