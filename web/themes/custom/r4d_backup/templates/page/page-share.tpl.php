<span class="share-toolbar__description"><?php print t('Share'); ?>:</span>

<ul class="share-toolbar__list">
  <li class="share-toolbar__list-item">
    <a href="#" class="addthis_button_facebook at300b pageShare share-toolbar__icon share-toolbar__icon--facebook" title="Facebook"></a>
  </li>
  <li class="share-toolbar__list-item">
    <a href="https://www.addthis.com/bookmark.php?v=300" class="addthis_button_twitter at300b pageShare share-toolbar__icon share-toolbar__icon--twitter" title="Tweet"></a>
  </li>
  <li class="share-toolbar__list-item">
    <a href="https://www.addthis.com/bookmark.php?v=300" class="addthis_button_google_plusone_share pageShare at300b share-toolbar__icon share-toolbar__icon--google" title="Google Plus"></a>
  </li>
  <li class="share-toolbar__list-item">
    <a href="https://www.addthis.com/bookmark.php?v=300" class="addthis_button_email at300b pageShare share-toolbar__icon share-toolbar__icon--email" title="Email"></a>
  </li>
  <li class="share-toolbar__list-item">
    <a href="javascript:window.print();" class="pageShare share-toolbar__icon share-toolbar__icon--print" title="Print this Page"></a>
  </li>
</ul>