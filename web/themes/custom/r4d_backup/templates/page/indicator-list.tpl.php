<div class="indicator_list">
  <h3 class="indicator_list-header"><?php print t($header_title); ?></h3>

  <div class="indicator_list-content clearfix">
    <?php foreach ($columns as $column) : ?>
      <div class="indicator_list-col">
        <?php foreach ($column as $category) : ?>
          <?php if (empty($category)) { continue; } ?>

          <div class="indicator_list-item <?php print $category['css_class']; ?>">
            <div class="category_box">
              <h2 class="indicator_list-item-name category_box--title"><?php print $category['short_name']; ?></h2>
              <h3 class="indicator_list-item-subtitle"><?php print $category['title_question']; ?></h3>

              <ul>
                <?php foreach ($category['indicators'] as $indicator) : ?>
                  <li class="indicator_list-item-link"><?php print $indicator['link']; ?></li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endforeach; ?>
  </div>
</div>
