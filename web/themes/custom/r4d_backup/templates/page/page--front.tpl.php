<?php if (!empty($page['banner_top'])): ?>
  <div class="banner-top-wrapper">
    <section id="banner-section-top">
        <?php print render($page['banner_top']); ?>
    </section>
  </div>
<?php endif; ?>

<?php if (!empty($page['browser_message'])): ?>
  <section id="browser-message" style="display: none;">
    <div class="main">
      <?php print render($page['browser_message']); ?>
    </div>
  </section>
<?php endif; ?>

<div role="document" class="page">
  <section class="site_header" ng-controller="NavController">
    <div class="site_header--inner main js-site_header--inner">
      <?php print $linked_logo; ?>

      <?php print theme('links__menu_top_quick_nav', array('links' => NULL, 'container' => 'ul', 'container_class' => array('top-quicknav', ' for-desktop'))); ?>

      <div class="hamburger-menu js-responsive-nav-trigger">
        <div class="hamburger-menu__content">
          <?php print theme("icons_hamburger"); ?>
          <?php print theme("icons_x"); ?>
          Menu
        </div>
      </div>

      <div class="hamburger-menu__links js-hamburger-menu__links">
        <?php print theme('links__menu_top_quick_nav', array('links' => NULL, 'container' => 'ul', 'container_class' => array('top-quicknav', ' for-mobile'))); ?>
        <?php print theme('links__main_menu', array('links' => $main_menu, 'container' => 'ul', 'container_class' => array('site-nav', ' js-site-nav'))); ?>
      </div>
    </div>

    <div class="nav_drawer nav_drawer--container js-nav_drawer--target">
      <?php print $_partials['typeahead_drawer']; ?>
      <?php print $_partials['indicator_drawer']; ?>
    </div>
  </section>

  <?php if (!empty($page['header'])): ?>
    <section id="header">
      <?php print render($page['header']); ?>
    </section>
  <?php endif; ?>

  <?php if ($messages): ?>
    <section id="messages">
      <div class="main">
        <?php print $messages; ?>
      </div>
    </section>
  <?php endif; ?>

  <div role="main" class="content-wrapper" <?php if ($is_front) : ?>ng-controller="HomeController" class="ng-scope"<?php endif; ?>>
    <section id="content">
      <?php if(!empty($page['masthead'])) : ?>
        <section id="masthead">
          <?php print render($page['masthead']); ?>
        </section>
      <?php endif; ?>

      <?php if (!empty($tabs)): ?>
        <div class="main-content">
          <?php print render($tabs); ?>
          <?php if (!empty($tabs2)): print render($tabs2); endif; ?>
        </div>
      <?php endif; ?>

      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>

      <?php print render($page['content']); ?>

    </section>

    <?php if (!empty($page['modal'])): ?>
      <div class="js-modal-scroll-open">
        <?php print render($page['modal']); ?>
      </div>
    <?php endif; ?>
  </div>

  <footer id="site-footer" role="contentinfo" class="site_footer js-site-footer">
    <div class="main">
      <section class="site_footer__newsletter js-newsletter-sticky">
        <div class="main">
          <h3 class="site_footer__newsletter-title"><?php print theme("icons_newsletter"); ?><?php print t('Sign up for our newsletter'); ?></h3>
          <?php print render($mailchimp_form); ?>
        </div>
      </section>
      <section class="site_footer__content">
        <div class="site_footer__links">
          <span class="site_footer__copyright">
              <?php print t('Copyright'); ?>
          </span>

          <?php print $footer_menu; ?>
        </div>
        <div class="site_footer__info">
          <div class="site_footer__info-item site_footer__contact-wrapper">
            <ul class="site_footer__contact">
              <li class="site_footer__contact-item"><?php print $site_mail; ?></li>
            </ul>
          </div>
          <div class="site_footer__info-item site_footer__twitter"><?php print $site_twitter; ?></div>
        </div>
        <div class="site_footer__info site_footer__download-data">
          <a href="#" target="_blank" class="download-data"><?php print theme('icons_download'); ?> <?php print $download_all_data_url; ?></a>
        </div>
      </section>
    </div>
  </footer>
</div>
