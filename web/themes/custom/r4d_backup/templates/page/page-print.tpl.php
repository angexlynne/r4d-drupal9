<span class="print-toolbar__description share-toolbar__description"><?php print t('Print'); ?>:</span>

<ul class="print-toolbar__list share-toolbar__list">
  <li class="print-toolbar__list-item share-toolbar__list-item">
    <a href="javascript:window.print();" class="pageShare print-toolbar__icon print-toolbar__icon--print share-toolbar__icon--print" title="Print this Page"></a>
  </li>
</ul>