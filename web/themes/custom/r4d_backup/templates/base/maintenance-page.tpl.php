<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js ie lt-ie10 lt-ie9 lt-ie8 ie7"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie lt-ie10 lt-ie9 ie8"> <![endif]-->
<!--[if IE 9 ]> <html class="no-js ie lt-ie10 ie9"> <![endif]-->
<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic,600italic,700italic,900italic&subset=vietnamese,latin-ext' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Neuton:300,400,400italic' rel='stylesheet' type='text/css'>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
<div role="document" class="page">
  <section class="site_header" ng-controller="NavController">
    <div class="site_header--inner main js-site_header--inner">
      <?php print $linked_logo; ?>

      <?php print theme('links__menu_top_quick_nav', array('links' => $top_quick_nav, 'container' => 'ul', 'container_class' => array('top-quicknav', ' for-desktop'))); ?>

      <div class="hamburger-menu js-responsive-nav-trigger">
        <div class="hamburger-menu__content">
          <?php print theme("icons_hamburger"); ?>
          <?php print theme("icons_x"); ?>
          Menu
        </div>
      </div>

      <div class="hamburger-menu__links js-hamburger-menu__links">
        <?php print theme('links__menu_top_quick_nav', array('links' => $top_quick_nav, 'container' => 'ul', 'container_class' => array('top-quicknav', ' for-mobile'))); ?>
        <?php print theme('links__main_menu', array('links' => $main_menu, 'container' => 'ul', 'container_class' => array('site-nav', ' js-site-nav'))); ?>
      </div>
    </div>

    <div class="nav_drawer nav_drawer--container js-nav_drawer--target">
      <?php print $_partials['typeahead_drawer']; ?>
      <?php print $_partials['indicator_drawer']; ?>
    </div>
  </section>

  <?php if (!$is_front && !empty($page['masthead'])): ?>
    <section id="masthead">
      <?php print render($page['masthead']); ?>
    </section>
  <?php endif; ?>

  <div role="main" class="content-wrapper">
    <section id="content">
      <div class="main">
        <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
        <p><?php print $content; ?></p>
      </div>
    </section>

    <?php if (!empty($page['site_page_notes'])): ?>
      <section class="site-page-notes">
        <div class="site-page-notes__container main">
          <a name="page-notes"></a>
          <?php print render($page['site_page_notes']); ?>
        </div>
      </section>
    <?php endif; ?>
  </div>


  <footer id="site-footer" role="contentinfo" class="site_footer js-site-footer">
    <div class="main">
      <section class="site_footer__newsletter js-newsletter-sticky">
        <div class="main">
          <h3 class="site_footer__newsletter-title"><?php print theme("icons_newsletter"); ?><?php print t('Sign up for our newsletter'); ?></h3>
          <?php print render($mailchimp_form); ?>
        </div>
      </section>
      <section class="site_footer__content">
        <div class="site_footer__links">
          <?php print $footer_menu; ?>
        </div>
        <div class="site_footer__info">
          <div class="site_footer__contact-wrapper">
            <ul class="site_footer__contact">
              <li class="site_footer__contact-item"><?php print $site_mail; ?></li>
            </ul>
          </div>
          <div class="site_footer__twitter"><?php print $site_twitter; ?></div>
        </div>
      </section>
    </div>
  </footer>
</div>

</body>
</html>
