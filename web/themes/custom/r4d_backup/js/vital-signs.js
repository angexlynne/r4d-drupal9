import React                from "react";
import {render}             from "react-dom";
import * as d3              from "d3";

import VizVitalsRange       from "./react/viz-vitals-range.jsx";
import VizVitalsBlock       from "./react/viz-vitals-block.jsx";
import VizVitalsBar         from "./react/viz-vitals-bar.jsx";
import VizVitalsBarPhc      from "./react/viz-vitals-bar-phc.jsx";

const convert2Number = value => Math.round(parseFloat(value));

// forEach polyFilled for IE11
document.querySelectorAll(".js-vital-sign-range").forEach((el)=>{

  const props = {
    lowValue: convert2Number(el.getAttribute("data-low") || 0),
    highValue: convert2Number(el.getAttribute("data-high") || 0),
    lowDisplayValue: el.getAttribute("data-display-low") || "0",
    highDisplayValue: el.getAttribute("data-display-high") || "0",
    lowValueLabel: el.getAttribute("data-low-label"),
    highValueLabel: el.getAttribute("data-high-label"),
    maxValue: parseInt(el.getAttribute("data-max") || 100),
    themeColor: el.getAttribute("data-color")
  };

  render(
    <VizVitalsRange { ...props} />,
    el
  );
});

// forEach polyFilled for IE11
document.querySelectorAll(".js-vital-sign-block").forEach((el)=>{
  const d = parseFloat(el.getAttribute("data-value") || 0);
  const displayValue = el.getAttribute("data-simple-value");
  const min = 0;
  const max = 4;

  // value is a range 0 -> 4
  const value = d < min ? min
    : d > max ? max
    : d;

  const props = {
    value,
    displayValue,
    themeColor: el.getAttribute("data-color")
  };

  render(
    <VizVitalsBlock { ...props} />,
    el
  );
});

// forEach polyFilled for IE11
document.querySelectorAll(".js-vital-sign-bar").forEach((el)=>{

  const props = {
    displayValue: el.getAttribute("data-display-value") || "0",
    value: parseFloat(el.getAttribute("data-value") || 0),
    maxValue: parseInt(el.getAttribute("data-max") || 100),
    themeColor: el.getAttribute("data-color") || null
  };

  if(el.getAttribute("data-type").toLowerCase() === "percent") {
    props.value = convert2Number(props.value * 100);
  } else {
    props.value = convert2Number(props.value);
  }

  render(
    <VizVitalsBar { ...props} />,
    el
  );
});

// forEach polyFilled for IE11
document.querySelectorAll(".js-vital-sign-bar-phc").forEach((el)=>{

  const props = {
    value: convert2Number(el.getAttribute("data-value") || 0),
    maxValue: parseInt(el.getAttribute("data-max") || 100)
  };

  render(
    <VizVitalsBarPhc { ...props} />,
    el
  );
});

// forEach polyFilled for IE11
document.querySelectorAll(".js-vital-sign-donut").forEach((el)=>{
  const value = convert2Number(el.getAttribute("data-value") || 0);
  const selector = d3.select(el);

  createDonut(selector,value);
});

function createDonut(selector,value) {
  const data = [100 - value, value];
  const height = 120;
  const width = 120;
  const radius = width / 2;
  const color = d3.scaleOrdinal(['#28265f','#8988a3']);

  const pie = d3.pie().value((d) => d).sort(null);
  const path = d3.arc().innerRadius(32).outerRadius(radius);

  const svg = selector
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .attr("class", 'viz-vitals-donut__svg');

  const g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

  g.selectAll(".arc")
    .data(pie(data))
    .enter()
    .append("g")
      .attr("class", "viz-vitals-donut__arc")
      .append("path")
      .attr("d", path)
      .attr("fill", function(d) { return color(d.data); });
}
