import $ from "jquery";


let state = {
  onlyTrailblazers: false
};

function setState(newState) {
  const oldState = state;
  state = Object.assign(state, newState);

  render(state, oldState);
}

function render(state, oldState) {

  // show hide the active section
  $(".js-country-listing__all").toggleClass("is-hidden", state.onlyTrailblazers);
  $(".js-country-listing__trailblazers").toggleClass("is-hidden", !state.onlyTrailblazers);

  // update the button text
  $(".js-country-listing__toggle").text(
    state.onlyTrailblazers ? "Show All Countries" : "Show Only Trailblazers"
  );
}

$(".js-country-listing__toggle").click(() => {
  setState({
    onlyTrailblazers: !state.onlyTrailblazers
  });
});

render(state);
