import React, { Component }   from "react";

import { string, number } from "prop-types";

export default class VizVitalsBar extends Component {
  static propTypes = {
    displayValue: string.isRequired,
    value: number.isRequired,
    maxValue: number.isRequired,
    themeColor: string
  }

  constructor(props) {
    super(props);

    this.state = {
      displayValue: "0",
      value: 0,
      maxValue: 100,
      themeColor: "blank"
    };
  }

  componentDidMount() {
    this.setState({...this.props});
  }

  render() {
    const { themeColor, value, maxValue, displayValue } = this.state;

    const themeClass = themeColor ? `viz-vitals-bar__value--${ themeColor }` : "";

    const valueStyles = { 
      minWidth: `${(value / maxValue * 100)}%` 
    };

    return(
      <div className="viz-vitals-bar" aria-label={ `${ displayValue }%` }>
        <div className="viz-vitals-bar__label viz-vitals-bar__label--min">0</div>
        <div className="viz-vitals-bar__label viz-vitals-bar__label--mid">{ maxValue / 2 }</div>
        <div className="viz-vitals-bar__label viz-vitals-bar__label--max">{ maxValue }</div>
        <div className={ `viz-vitals-bar__value ${themeClass}` } style={ valueStyles }>
          { displayValue }
        </div>
      </div>
    );
  }
}
