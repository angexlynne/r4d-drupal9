import React, { Component }   from "react";

import { string, number } from "prop-types";

export default class VizVitalsBlock extends Component {
  static propTypes = {
    value: number.isRequired,
    displayValue: string.isRequired,
    themeColor: string.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      value: 0,
      themeColor: ""
    };
  }

  componentDidMount() {
    this.setState({...this.props});
  }

  /** takes in a value 1-4 and color
  * returns a array with four items
  */
  renderChart(value, color) {
    return Array.apply(null, Array(4)).map((e,i) => {
      const themeClass = i < Math.floor(value) ? `viz-vitals-block__chart--${ color }` : "";
      return (
        <div key={ i } className={ `viz-vitals-block__chart ${ themeClass }` }></div>
      );
    });
  }

  render() {
    const { themeColor, value } = this.state;
    const { displayValue } = this.props;

    return(
      <div className="viz-vitals-block">
        <div className="viz-vitals-block__value">{ displayValue }</div>
        { this.renderChart(value, themeColor) }
      </div>
    );
  }
}
