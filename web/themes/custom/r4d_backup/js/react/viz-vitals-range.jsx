import React, { Component }   from "react";

import { string, number } from "prop-types";

export default class VizVitalsRange extends Component {
  static propTypes = {
    lowValue: number.isRequired,
    highValue: number.isRequired,
    lowDisplayValue: string.isRequired,
    highDisplayValue: string.isRequired,
    lowValueLabel: string,
    highValueLabel: string,
    maxValue: number.isRequired,
    themeColor: string.isRequired
  }

  constructor(props) {
    super(props);

    const { maxValue, lowValueLabel, highValueLabel } = this.props;

    this.state = {
      lowValue: 0,
      highValue: maxValue,
      lowDisplayValue: "0",
      highDisplayValue: `${ maxValue }`,
      lowValueLabel: lowValueLabel || "",
      highValueLabel: highValueLabel || "",
      maxValue: maxValue,
      themeColor: ""
    };
  }

  componentDidMount() {
    const { maxValue, highValue } = this.props;

    // make sure the range includes the max value
    const max = maxValue < highValue ? Math.round(highValue / 50) * 50 : maxValue;

    this.setState({...this.props, maxValue: max });
  }

  render() {
    const { themeColor, lowValue, highValue,lowDisplayValue, highDisplayValue, lowValueLabel, highValueLabel, maxValue } = this.state;

    const themeClass = themeColor ? `viz-vitals-range--${ themeColor }` : "";

    const lowPos = lowValue / maxValue * 100;
    const maxPos = highValue / maxValue * 100;

    const valueStyles = { 
      left: `${ lowPos }%`, 
      right: `${ 100 - maxPos }%` 
    };

    const rangeClass = maxPos - lowPos < 9 ? "viz-vitals-range--narrow" : "";

    return(
      <div className={ `viz-vitals-range ${ themeClass } ${ rangeClass }` }>
        <div className="viz-vitals-range__bar">
          <div className="viz-vitals-range__label viz-vitals-range__label--min">0</div>
          <div className="viz-vitals-range__label viz-vitals-range__label--max">{ maxValue }</div>
        </div>
        <div className="viz-vitals-range__values" style={ valueStyles }>
          <div className="viz-vitals-range__range-bar viz-vitals-range__range-bar--bottom"></div>
          <div className="viz-vitals-range__handle-low">
            <span className="viz-vitals-range__low-value" aria-label="{ lowValueLabel }">{ lowDisplayValue }</span>
            <span className="viz-vitals-range__low-label">{ lowValueLabel }</span>
          </div>
          <div 
            className="viz-vitals-range__handle-high">
            <span className="viz-vitals-range__high-value" aria-label="{ highValueLabel }">{ highDisplayValue }</span>
            <span className="viz-vitals-range__high-label">{ highValueLabel }</span>
          </div>
          <div className="viz-vitals-range__range-bar viz-vitals-range__range-bar--top"></div>
        </div>
      </div>
    );
  }
}
