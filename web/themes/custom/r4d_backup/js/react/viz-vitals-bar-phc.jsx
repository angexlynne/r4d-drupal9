import React, { Component }   from "react";

import { number } from "prop-types";

export default class VizVitalsBarPhc extends Component {
  static propTypes = {
    value: number.isRequired,
    maxValue: number.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      value: 0,
      maxValue: 100
    };
  }

  componentDidMount() {
    this.setState({...this.props});
  }

  render() {
    const { value, maxValue } = this.state;


    const valueStyles = { 
      width: `${(value / maxValue * 100)}%` 
    };

    return(
      <div className="viz-vitals-bar-phc" aria-label={ `${ value }% on PHC` }>
        <div className="viz-vitals-bar-phc__value" style={ valueStyles }>
          <span className="viz-vitals-bar-phc__display-value">{ value }</span><span className="viz-vitals-bar-phc__label">% on PHC</span>
        </div>
      </div>
    );
  }
}
