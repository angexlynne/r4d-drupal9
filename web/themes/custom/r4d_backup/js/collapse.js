import $ from "jquery";

$(".js-collapse").each(function() {
  const $collapse = $(this);
  const $collapseBody = $collapse.find(".js-collapse-body").first();

  if ($collapseBody.length === 0) {
    console.warn(`.js-collapse-body not found!`);
    return false;
  }

  $collapse.find(".js-collapse-toggle").click(function(){
    $collapse.toggleClass("is-open");
    $collapseBody.toggleClass("is-open");

    if ($collapseBody.hasClass("is-open")) {
      $collapseBody.slideDown();
    } else {
      $collapseBody.slideUp();
    }
  });

  if(!$collapse.hasClass("is-open")) {
    $collapseBody.slideUp();
  }
});
