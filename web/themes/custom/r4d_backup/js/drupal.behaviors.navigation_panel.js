/* global Drupal */
import $ from "jquery";

Drupal.behaviors.navigation_panel = {
  attach: function(context, settings) {
    if ($("body.show-navigation-panel").length) {
      var offset = $(".site_header--inner").offset().left;
      var $navigation_panel = $("#navigation-panel");
      $navigation_panel.toggleClass("show-over", $(window).width() <= 640);
      $navigation_panel.css("margin-left", offset);
      // TODO this could be setted in backend
      $navigation_panel
        .find("a.active")
        .closest(".accordion-items__item.js-np-collapse")
        .addClass("init-open")
        .click();
      var $site_header = $("section.site_header");
      var top = $site_header.position().top + $site_header.height();
      $(window).scroll(function() {
        var fixed = $(this).scrollTop() > top;
        $navigation_panel.toggleClass("fixed", fixed);
      });

      $navigation_panel.click(function(e) {
        var $el = $(this);
        if (!$el.hasClass("show-over")) {
          return;
        }
        var is_open = $el.hasClass("open");
        var close = false;
        if (is_open) {
          close = e.pageY - $(e.currentTarget).offset().top < 40;
        }

        $el.toggleClass("open", !close);
      });
    }

    $(".js-np-collapse").each(function() {
      var $collapse = $(this);
      var $collapseBody = $collapse.find(".js-np-collapse-body").first();
      $collapseBody.hide();
      if ($collapse.hasClass("init-open")) {
        $collapse.removeClass("init-open");
        $collapse.addClass("is-open");
        $collapseBody.addClass("is-open").show();

        var $active_item = $("#navigation-panel").find("a.active");
        if ($active_item.length) {
          var $container = $(
            ".navigation-panel-wrapper .component .main-container"
          );
          var scrollTop = $container.scrollTop() + $active_item.position().top;
          $container.scrollTop(scrollTop);
        }
      }

      $collapse.find(".js-np-collapse-toggle").click(function() {
        $collapse.toggleClass("is-open");
        $collapseBody.toggleClass("is-open");

        if ($collapseBody.hasClass("is-open")) {
          $collapseBody.slideDown();
          var $container = $(
            ".navigation-panel-wrapper .component .main-container"
          );
          var scrollTop = $container.scrollTop() + $collapse.position().top;
          $container.animate({ scrollTop: scrollTop });
        } else {
          $collapseBody.slideUp();
        }
      });
    });

    $(window).resize(function() {
      var offset = $(".site_header--inner").offset().left;
      var $navigation_panel = $("#navigation-panel");
      $navigation_panel.css("margin-left", offset);
      $navigation_panel.toggleClass("show-over", $(window).width() <= 640);
      $navigation_panel.removeClass("open");
    });
  }
};
