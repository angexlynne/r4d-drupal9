import "jquery-match-height";
import "./drupal.behaviors.r4d.js";
import "./drupal.behaviors.facet_card";
import "./drupal.behaviors.navigation_panel";
import "./modal.js";
import "./collapse.js";
import "./country-toggle.js";
import "./country-listing.js";
import "./references.js";
import "./drupal.behaviors.media.js"

// React Apps for Vital Signs on Country page
import "./vital-signs.js";
