/* global Drupal */
import $ from "jquery";

Drupal.behaviors.facet_card = {
  attach: function (context, settings) {
    $(".component--facet-card-listing").find(".component_filter_option").click(function () {
      var $option = $(this);
      var $main = $option.parents(".main");
      var tid = $option.data("tid");
      if (tid === "all") {
        $main.find(".card--card-listing").show();
        $option.parent().find(".active").removeClass("active");
        $option.addClass("active");
        return;
      }
      if($option.hasClass("active")){
        $option.removeClass("active");
        $main.find(".card--card-listing").show();
        $main.find("[data-tid=all]").addClass("active");
      } else {
        $option.parent().find(".active").removeClass("active");
        $option.addClass("active");
        $main.find(".card--card-listing").not(".card__card-listing-tid-"+tid).hide();
        $main.find(".card--card-listing.card__card-listing-tid-"+tid).show();
      }
    });
  }
};
