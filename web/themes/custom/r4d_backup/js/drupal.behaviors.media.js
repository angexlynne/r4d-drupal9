import $ from "jquery";

Drupal.behaviors.media = {
  attach: function (context, settings) {
    // Change width of media iframe
    $(".node-type-landing-page .media-youtube-player, .node-type-landing-page .media-vimeo-player").css({"width":"100%", "height":"260px"});

    $(".node-type-campaign .media-youtube-player, .node-type-campaign .media-vimeo-player").css({"width":"100%", "height":"260px"});
  }
};
