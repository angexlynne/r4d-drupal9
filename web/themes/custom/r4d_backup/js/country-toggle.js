import $ from "jquery";

export default (function(window = window, document = document) {

  // should only be two sections
  const $sections = $(".js-country-section");
  let focus = setTimeout(()=> null,1);

  if($sections.length < 2) {
    return;
  }

  $sections.first().addClass("is-open");
  $sections.last().removeClass("is-open");

  animateViz();

  $sections.each(function() {
    const $el = $(this);
    $el.find(".js-country-section-button").click(function(e) {
      e.preventDefault();
      
      $sections.toggleClass("is-open");

      // wait for animations to finish
      // before focusing on the new section
      clearTimeout(focus);
      
      focus = setTimeout(function(){
        $(".js-country-section.is-open").find(".js-country-section-button").focus();
      },600);
    });
  });

  // $sections
  //   .first()
  //   .one("click", ".js-country-section-button", animateViz);

  function animateViz() {
    const event = document.createEvent("event");
    event.initEvent("show-vital-signs", true, true);
    setTimeout(function(){
      document.querySelector("body").dispatchEvent(event);
    },600);
  }
})(window, document);
