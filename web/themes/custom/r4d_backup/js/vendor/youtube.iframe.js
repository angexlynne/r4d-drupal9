
if (!window['YT']) {var YT = {loading: 0,loaded: 0};}if (!window['YTConfig']) {var YTConfig = {'host': 'http://www.youtube.com'};}if (!YT.loading) {YT.loading = 1;(function(){var l = [];YT.ready = function(f) {if (YT.loaded) {f();} else {l.push(f);}};window.onYTReady = function() {YT.loaded = 1;for (var i = 0; i < l.length; i++) {try {l[i]();} catch (e) {}}};YT.setConfig = function(c) {for (var k in c) {if (c.hasOwnProperty(k)) {YTConfig[k] = c[k];}}};var a = document.createElement('script');a.type = 'text/javascript';a.id = 'www-widgetapi-script';a.src = 'https:' + '//s.ytimg.com/yts/jsbin/www-widgetapi-vfl1Omgyb/www-widgetapi.js';a.async = true;var b = document.getElementsByTagName('script')[0];b.parentNode.insertBefore(a, b);})();}

var player;
function onYouTubePlayerAPIReady() {
  autoplay_desktop = 0;
  if (window.innerWidth >= 1200) {
    autoplay_desktop = 1;
  }
  player = new YT.Player('youtube-video', {
    playerVars: { 'autoplay': 1, 'controls': 0,'autohide':1,'wmode':'opaque' },
    events: {
    'onReady': onPlayerReady,
    'onStateChange': onPlayerStateChange
    }
  });
}
// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  if (window.innerWidth >= 1200) {
    player.mute();
    window.addEventListener('scroll', function(){checkScroll(event)}, false);
    window.addEventListener('resize', function(){checkScroll(event)}, false);
    //check at least once so you don't have to wait for scrolling for the video to start if the cursor height already set
    window.addEventListener('load', function(){checkScroll(event)}, false);
  }
}

function onPlayerStateChange(event) {
  if (event.data == YT.PlayerState.PLAYING) {

  } else {
    //console.log("event paused");
  }
};

function stopVideo() {
  player.stopVideo();
};

function playVideo() {
  player.playVideo();
};

function pauseVideo() {
  player.pauseVideo();
};



//play when video is visible
var videos = document.getElementsByTagName("iframe"), fraction = 0.8;

function checkScroll(event) {
    var top = (window.pageYOffset)
    if (top >= 140 && top <= 1090) {
      var el = document.querySelector('.home__video__media__overlay');
      if (el && !el.classList.contains('fade-out')){
        el.classList.add('fade-out');
      }
      playVideo();
    } else {
      pauseVideo();
    }
};