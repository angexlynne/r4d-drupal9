import $ from "jquery";

// open the modal
$(".js-modal-trigger").click(function(){
  const selector = $(this).data("modal");
  const indicator_id = $(this).data("indicator-id");

  if ($(selector).length > 0) {
    $(selector).addClass("is-open");
    $("body").addClass("modal-open");
    $("[data-button-indicator-id='"+indicator_id+"']").click();
    // wait for animation
    setTimeout(function(){
      $("button.js-modal-close").first().focus();
    },300);
  }
  else {
    console.warn(`${selector} not found!`);
  }
});


// close the modal
$(".js-modal-close").click(function() {
  const modal = $(this).closest(".modal");
  modal.removeClass("is-open");
  $("body").removeClass("modal-open");
});

if(document.querySelectorAll(".js-modal-scroll-open").length) {
  window.addEventListener('scroll', showModal, false);
}

function showModal(e) {
  if(window.scrollY > 10){
    window.removeEventListener('scroll', showModal, false);
    setTimeout(()=> {
      document.querySelector(".js-modal-scroll-open").classList.remove("js-modal-scroll-open");
    }, 4000);
  }
}
