import $ from "jquery";

export default (function(window = window, document = document) {

  if ($(".js-indicator-reference").length && $(".js-references-target").length) {
    $(".js-indicator-reference").each(function(i) {
      $(this).text(i + 1);
      $(".js-references-target").append("<li>" + $(this).data("reference") + "</li>");
    });
    $(".js-references-target").parent().find("h2").show();
  }

})(window, document);
