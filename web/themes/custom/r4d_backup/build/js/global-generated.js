(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _jquery2.default)(".js-collapse").each(function () {
  var $collapse = (0, _jquery2.default)(this);
  var $collapseBody = $collapse.find(".js-collapse-body").first();

  if ($collapseBody.length === 0) {
    console.warn(".js-collapse-body not found!");
    return false;
  }

  $collapse.find(".js-collapse-toggle").click(function () {
    $collapse.toggleClass("is-open");
    $collapseBody.toggleClass("is-open");

    if ($collapseBody.hasClass("is-open")) {
      $collapseBody.slideDown();
    } else {
      $collapseBody.slideUp();
    }
  });

  if (!$collapse.hasClass("is-open")) {
    $collapseBody.slideUp();
  }
});

},{"jquery":"jquery"}],2:[function(require,module,exports){
"use strict";

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var state = {
  onlyTrailblazers: false
};

function setState(newState) {
  var oldState = state;
  state = Object.assign(state, newState);

  render(state, oldState);
}

function render(state, oldState) {

  // show hide the active section
  (0, _jquery2.default)(".js-country-listing__all").toggleClass("is-hidden", state.onlyTrailblazers);
  (0, _jquery2.default)(".js-country-listing__trailblazers").toggleClass("is-hidden", !state.onlyTrailblazers);

  // update the button text
  (0, _jquery2.default)(".js-country-listing__toggle").text(state.onlyTrailblazers ? "Show All Countries" : "Show Only Trailblazers");
}

(0, _jquery2.default)(".js-country-listing__toggle").click(function () {
  setState({
    onlyTrailblazers: !state.onlyTrailblazers
  });
});

render(state);

},{"jquery":"jquery"}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  var window = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window;
  var document = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;


  // should only be two sections
  var $sections = (0, _jquery2.default)(".js-country-section");
  var focus = setTimeout(function () {
    return null;
  }, 1);

  if ($sections.length < 2) {
    return;
  }

  $sections.first().addClass("is-open");
  $sections.last().removeClass("is-open");

  animateViz();

  $sections.each(function () {
    var $el = (0, _jquery2.default)(this);
    $el.find(".js-country-section-button").click(function (e) {
      e.preventDefault();

      $sections.toggleClass("is-open");

      // wait for animations to finish
      // before focusing on the new section
      clearTimeout(focus);

      focus = setTimeout(function () {
        (0, _jquery2.default)(".js-country-section.is-open").find(".js-country-section-button").focus();
      }, 600);
    });
  });

  // $sections
  //   .first()
  //   .one("click", ".js-country-section-button", animateViz);

  function animateViz() {
    var event = document.createEvent("event");
    event.initEvent("show-vital-signs", true, true);
    setTimeout(function () {
      document.querySelector("body").dispatchEvent(event);
    }, 600);
  }
}(window, document);

},{"jquery":"jquery"}],4:[function(require,module,exports){
"use strict";

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

Drupal.behaviors.facet_card = {
  attach: function attach(context, settings) {
    (0, _jquery2.default)(".component--facet-card-listing").find(".component_filter_option").click(function () {
      var $option = (0, _jquery2.default)(this);
      var $main = $option.parents(".main");
      var tid = $option.data("tid");
      if (tid === "all") {
        $main.find(".card--card-listing").show();
        $option.parent().find(".active").removeClass("active");
        $option.addClass("active");
        return;
      }
      if ($option.hasClass("active")) {
        $option.removeClass("active");
        $main.find(".card--card-listing").show();
        $main.find("[data-tid=all]").addClass("active");
      } else {
        $option.parent().find(".active").removeClass("active");
        $option.addClass("active");
        $main.find(".card--card-listing").not(".card__card-listing-tid-" + tid).hide();
        $main.find(".card--card-listing.card__card-listing-tid-" + tid).show();
      }
    });
  }
}; /* global Drupal */

},{"jquery":"jquery"}],5:[function(require,module,exports){
"use strict";

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

Drupal.behaviors.media = {
  attach: function attach(context, settings) {
    // Change width of media iframe
    (0, _jquery2.default)(".node-type-landing-page .media-youtube-player, .node-type-landing-page .media-vimeo-player").css({ "width": "100%", "height": "260px" });

    (0, _jquery2.default)(".node-type-campaign .media-youtube-player, .node-type-campaign .media-vimeo-player").css({ "width": "100%", "height": "260px" });
  }
};

},{"jquery":"jquery"}],6:[function(require,module,exports){
"use strict";

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

Drupal.behaviors.navigation_panel = {
  attach: function attach(context, settings) {
    if ((0, _jquery2.default)("body.show-navigation-panel").length) {
      var offset = (0, _jquery2.default)(".site_header--inner").offset().left;
      var $navigation_panel = (0, _jquery2.default)("#navigation-panel");
      $navigation_panel.toggleClass("show-over", (0, _jquery2.default)(window).width() <= 640);
      $navigation_panel.css("margin-left", offset);
      // TODO this could be setted in backend
      $navigation_panel.find("a.active").closest(".accordion-items__item.js-np-collapse").addClass("init-open").click();
      var $site_header = (0, _jquery2.default)("section.site_header");
      var top = $site_header.position().top + $site_header.height();
      (0, _jquery2.default)(window).scroll(function () {
        var fixed = (0, _jquery2.default)(this).scrollTop() > top;
        $navigation_panel.toggleClass("fixed", fixed);
      });

      $navigation_panel.click(function (e) {
        var $el = (0, _jquery2.default)(this);
        if (!$el.hasClass("show-over")) {
          return;
        }
        var is_open = $el.hasClass("open");
        var close = false;
        if (is_open) {
          close = e.pageY - (0, _jquery2.default)(e.currentTarget).offset().top < 40;
        }

        $el.toggleClass("open", !close);
      });
    }

    (0, _jquery2.default)(".js-np-collapse").each(function () {
      var $collapse = (0, _jquery2.default)(this);
      var $collapseBody = $collapse.find(".js-np-collapse-body").first();
      $collapseBody.hide();
      if ($collapse.hasClass("init-open")) {
        $collapse.removeClass("init-open");
        $collapse.addClass("is-open");
        $collapseBody.addClass("is-open").show();

        var $active_item = (0, _jquery2.default)("#navigation-panel").find("a.active");
        if ($active_item.length) {
          var $container = (0, _jquery2.default)(".navigation-panel-wrapper .component .main-container");
          var scrollTop = $container.scrollTop() + $active_item.position().top;
          $container.scrollTop(scrollTop);
        }
      }

      $collapse.find(".js-np-collapse-toggle").click(function () {
        $collapse.toggleClass("is-open");
        $collapseBody.toggleClass("is-open");

        if ($collapseBody.hasClass("is-open")) {
          $collapseBody.slideDown();
          var $container = (0, _jquery2.default)(".navigation-panel-wrapper .component .main-container");
          var scrollTop = $container.scrollTop() + $collapse.position().top;
          $container.animate({ scrollTop: scrollTop });
        } else {
          $collapseBody.slideUp();
        }
      });
    });

    (0, _jquery2.default)(window).resize(function () {
      var offset = (0, _jquery2.default)(".site_header--inner").offset().left;
      var $navigation_panel = (0, _jquery2.default)("#navigation-panel");
      $navigation_panel.css("margin-left", offset);
      $navigation_panel.toggleClass("show-over", (0, _jquery2.default)(window).width() <= 640);
      $navigation_panel.removeClass("open");
    });
  }
}; /* global Drupal */

},{"jquery":"jquery"}],7:[function(require,module,exports){
"use strict";

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

var _enquire = require("enquire.js");

var _enquire2 = _interopRequireDefault(_enquire);

require("jquery-match-height");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

Drupal.behaviors.r4d = {
  attach: function attach(context, settings) {
    if ((0, _jquery2.default)(".node-type-homepage").length) {
      (0, _jquery2.default)("#header .close").click(function () {
        (0, _jquery2.default)("#header").slideUp("fast");
      });
    }
    (0, _jquery2.default)("input, select, textarea").on("focus blur", function (event) {
      (0, _jquery2.default)("meta[name=viewport]").attr("content", "width=device-width,initial-scale=1,maximum-scale=" + (event.type == "blur" ? 10 : 1));
    });
    (0, _jquery2.default)(window).load(function () {
      if ((0, _jquery2.default)(".js-compare-table__pagination-next").length && (0, _jquery2.default)(".js-compare-table__pagination-prev").length) {
        (0, _jquery2.default)(".js-compare-table__pagination-prev").hover(function () {
          (0, _jquery2.default)(this).parent().addClass("compare-table__pagination--hover");
        }, function () {
          (0, _jquery2.default)(this).parent().removeClass("compare-table__pagination--hover");
        });
        (0, _jquery2.default)(".js-compare-table__pagination-next").hover(function () {
          (0, _jquery2.default)(this).parent().addClass("compare-table__pagination--hover");
        }, function () {
          (0, _jquery2.default)(this).parent().removeClass("compare-table__pagination--hover");
        });
      }
    });

    (0, _jquery2.default)(".image_pull--center").each(function () {
      (0, _jquery2.default)(this).wrap('<div class="image_pull--center_wrapper"></div>');
    });

    (0, _jquery2.default)(".centered_content_image_890").each(function () {
      (0, _jquery2.default)(this).wrap('<div class="image_pull--center_wrapper"></div>');
    });

    (0, _jquery2.default)(".hero_content_image_1200").each(function () {
      (0, _jquery2.default)(this).wrap('<div class="image_pull--center_wrapper"></div>');
    });

    //Article Content Body - add class when right rail exists
    (0, _jquery2.default)(".right_rail").prev(".content_body").addClass("with-right-rail");
    (0, _jquery2.default)(".right_rail").siblings(".related_tags").addClass("with-right-rail");
    (0, _jquery2.default)(".right_rail").parents("#content").addClass("with-right-rail");

    // Show / Hide
    (0, _jquery2.default)(".js-accordion-click").click(function () {
      (0, _jquery2.default)(".js-accordion-target").slideToggle("slow");
      (0, _jquery2.default)(".masthead_tag--morelink").hide();
    });

    (0, _jquery2.default)(".js-accordion-click2").click(function () {
      (0, _jquery2.default)(".js-accordion-target").slideToggle("slow");
      (0, _jquery2.default)(".masthead_tag--morelink").show();
    });

    (0, _jquery2.default)(document).ajaxSend(function (e, xhr, settings) {
      if (settings.extraData._triggering_element_name == "newsletter-submit") {
        (0, _jquery2.default)("form#r4d-signup-mailchimp-form").fadeTo("150", ".5");
      }
    }).ajaxComplete(function (e, xhr, settings) {
      if (settings.extraData._triggering_element_name == "newsletter-submit") {
        (0, _jquery2.default)("form#r4d-signup-mailchimp-form").fadeTo("150", "1");
      }
    });
    // unbind all events attached to document
    (0, _jquery2.default)(document).off();

    /** vital stats responsiveness **/
    if ((0, _jquery2.default)(".vital-stats__list").length && !(0, _jquery2.default)("html").not("#print-preview")) {
      var $statFeatured = (0, _jquery2.default)(".js-featured-vital-stats"),
          $statHidden = (0, _jquery2.default)(".js-hidden-vital-stats");

      _enquire2.default.register("screen and (max-width: 990px)", {
        match: function match() {
          $statFeatured.find(".vital-stats__item:nth-of-type(3)").prependTo($statHidden);
          var $parentElement = $statHidden.find(".vital-stats__item:nth-of-type(1)"),
              $orderElement = $parentElement.find(".vital-stats__value");

          $orderElement.prependTo($parentElement);
        },
        unmatch: function unmatch() {
          $statHidden.find(".vital-stats__item:nth-of-type(1)").appendTo($statFeatured);
          var $parentElement = $statFeatured.find(".vital-stats__item:nth-of-type(3)"),
              $orderElement = $parentElement.find(".vital-stats__value");
          $orderElement.appendTo($parentElement);
        }
      }).register("screen and (max-width: 640px)", {
        match: function match() {
          $statFeatured.find(".vital-stats__item:nth-of-type(2)").prependTo($statHidden);
          var $parentElement = $statHidden.find(".vital-stats__item:nth-of-type(1)"),
              $orderElement = $parentElement.find(".vital-stats__value");

          $orderElement.prependTo($parentElement);
        },
        unmatch: function unmatch() {
          $statHidden.find(".vital-stats__item:nth-of-type(1)").appendTo($statFeatured);
          var $parentElement = $statFeatured.find(".vital-stats__item:nth-of-type(2)"),
              $orderElement = $parentElement.find(".vital-stats__value");
          $orderElement.appendTo($parentElement);
        }
      }).register("screen and (max-width: 540px)", {
        match: function match() {
          $statHidden.find(".vital-stats__item").each(function () {
            var $currentItem = (0, _jquery2.default)(this),
                $orderElement = $currentItem.find(".vital-stats__value");
            $orderElement.appendTo($currentItem);
          });
        },
        unmatch: function unmatch() {
          $statHidden.find(".vital-stats__item").each(function () {
            var $currentItem = (0, _jquery2.default)(this),
                $orderElement = $currentItem.find(".vital-stats__value");
            $orderElement.prependTo($currentItem);
          });
        }
      });
    }

    //set classes on HTML based on media query
    _enquire2.default.register("screen and (min-width: 1081px)", {
      match: function match() {
        if ((0, _jquery2.default)("html").hasClass("hamburger-menu-active")) {
          (0, _jquery2.default)(".js-responsive-nav-trigger").click();
        }
        (0, _jquery2.default)("html").addClass("view-desktop-nav");
      },
      unmatch: function unmatch() {
        (0, _jquery2.default)("html").removeClass("view-desktop-nav");
      }
    }).register("screen and (min-width: 801px)", {
      match: function match() {
        (0, _jquery2.default)("html").addClass("view-desktop");
      },
      unmatch: function unmatch() {
        (0, _jquery2.default)("html").removeClass("view-desktop");
      }
    });

    /** Add Padding top to body tag when user is logged in **/
    bodyTag();
    (0, _jquery2.default)(window).resize(function () {
      bodyTag();
    });
    function bodyTag() {
      var $toolBarMenu = (0, _jquery2.default)(".toolbar-menu"),
          toolBarMenuHeight = "";
      if ($toolBarMenu.length) {
        toolBarMenuHeight = $toolBarMenu.outerHeight();
        (0, _jquery2.default)("body").css("padding-top", toolBarMenuHeight);
      }
    }

    /** Responsive Nav **/
    responsiveNav();
    function responsiveNav() {

      var $linkClone = "",
          $prependCloneTo = "",
          navDrawerTriggerClass = "js-nav_drawer-trigger",
          navDrawerTargetClass = "js-nav_drawer--target";

      //clone and prepend landing page link to dropdown menu
      (0, _jquery2.default)(".js-has-children").each(function () {
        var $link = (0, _jquery2.default)(this).find(".site-nav__item-link");
        if ($link.attr("href") === "" || $link.attr("href") === "#") {
          return;
        } else {
          $linkClone = $link.clone().removeAttr("class").addClass("site-subnav__item-link");
          $prependCloneTo = (0, _jquery2.default)(this).find(".site-subnav");
          $linkClone.wrap('<li class="site-subnav__item cloned"></li>').parent().prependTo($prependCloneTo);
        }
      });

      //insert selected nav_drawer to it's respective link triggers when window is resized
      (0, _jquery2.default)("." + navDrawerTriggerClass).each(function () {
        if ((0, _jquery2.default)(this).is(".selected")) {
          (0, _jquery2.default)("." + navDrawerTargetClass).insertAfter((0, _jquery2.default)(this));
        }
      });
      //insert nav_drawer to it's respective link triggers on click
      (0, _jquery2.default)(document).on("click", "." + navDrawerTriggerClass, function () {
        (0, _jquery2.default)("." + navDrawerTargetClass).insertAfter((0, _jquery2.default)(this));
      });

      var navTriggerClass = "js-responsive-nav-trigger",
          subnavTriggerClass = "js-responsive-subnav-trigger";

      (0, _jquery2.default)(document).on("click", "." + navTriggerClass, function () {
        (0, _jquery2.default)("html").toggleClass("hamburger-menu-active");
        (0, _jquery2.default)(this).toggleClass("is-active");
        (0, _jquery2.default)(window).trigger("resize");
      });

      (0, _jquery2.default)(document).on("click", "." + subnavTriggerClass, function () {
        (0, _jquery2.default)(this).toggleClass("is-active");
        return false;
      });
    }

    /** Sticky Newsletter section **/
    // stickyNewsletter();
    // $(window).scroll(function () {
    //   stickyNewsletter();
    // });
    // function stickyNewsletter() {
    //   var stickyElementClass = "js-newsletter-sticky",
    //     scrollTop = $(window).scrollTop(),
    //     documentHeight = $(document).height(),
    //     windowHeight = $(window).height(),
    //     scrollBottom = parseInt(windowHeight) + parseInt(scrollTop),
    //     stickyIntersection = documentHeight - $(".js-site-footer").outerHeight();

    //   enquire.register("screen and (min-width: 801px)", {
    //     match: function() {
    //       if(stickyIntersection >= scrollBottom) {
    //         $("." + stickyElementClass).addClass("am-stuck");
    //       } else {
    //         $("." + stickyElementClass).removeClass("am-stuck");
    //       }
    //     },
    //     unmatch: function() {
    //       $("." + stickyElementClass).removeClass("am-stuck");
    //     }
    //   });
    // }

    /** Sticky Compare Legend **/
    //stickyLegend();
    (0, _jquery2.default)(window).on("load scroll resize", function () {
      stickyLegend();
    });

    function stickyLegend() {
      var stickyWithinClass = "",
          stickyLegendClass = "";

      if ((0, _jquery2.default)("body").hasClass("node-type-indicator")) {
        stickyWithinClass = "js-indicator--chart";
        stickyLegendClass = "js-indicator--chart__legend";
      } else if ((0, _jquery2.default)("body").hasClass("node-type-comparison")) {
        stickyWithinClass = "js-compare-chart";
        stickyLegendClass = "js-compare-chart--legend";
      } else {
        return;
      }

      var stickyLegendTriggerClass = "js-legend-mobile--trigger",
          scrollTop = (0, _jquery2.default)(window).scrollTop(),
          stickyWithinOffset = (0, _jquery2.default)("." + stickyWithinClass).offset(),
          stickyStartAt = stickyWithinOffset.top,
          stickyEndAt = stickyStartAt + parseInt((0, _jquery2.default)("." + stickyWithinClass).outerHeight());

      if (stickyStartAt <= scrollTop && stickyEndAt >= scrollTop + 185) {
        if ((0, _jquery2.default)("body").hasClass("node-type-indicator")) {
          (0, _jquery2.default)("." + stickyLegendClass).css("top", scrollTop - stickyStartAt + parseInt((0, _jquery2.default)(".indicator_sum-title-sticky-wrap").outerHeight()));
          (0, _jquery2.default)("." + stickyLegendTriggerClass).css("top", scrollTop - stickyStartAt + 35 + parseInt((0, _jquery2.default)(".indicator_sum-title-sticky-wrap").outerHeight()));
        } else {
          (0, _jquery2.default)("." + stickyLegendClass).css("top", scrollTop - stickyStartAt);
          (0, _jquery2.default)("." + stickyLegendTriggerClass).css("top", scrollTop - stickyStartAt + 35);
        }
      } else {
        (0, _jquery2.default)("." + stickyLegendClass).css("top", "");
        (0, _jquery2.default)("." + stickyLegendTriggerClass).css("top", "");
      }
    }

    /** match height deep dive tiles **/
    (0, _jquery2.default)(".js-data_tile").matchHeight();

    if ((0, _jquery2.default)(".indicator_box").length && !(0, _jquery2.default)("body").hasClass("node-type-indicator-library")) {
      (0, _jquery2.default)(".indicator_box").matchHeight();
    }
  }
}; /* global Drupal */

},{"enquire.js":"enquire.js","jquery":"jquery","jquery-match-height":"jquery-match-height"}],8:[function(require,module,exports){
"use strict";

require("jquery-match-height");

require("./drupal.behaviors.r4d.js");

require("./drupal.behaviors.facet_card");

require("./drupal.behaviors.navigation_panel");

require("./modal.js");

require("./collapse.js");

require("./country-toggle.js");

require("./country-listing.js");

require("./references.js");

require("./drupal.behaviors.media.js");

require("./vital-signs.js");

},{"./collapse.js":1,"./country-listing.js":2,"./country-toggle.js":3,"./drupal.behaviors.facet_card":4,"./drupal.behaviors.media.js":5,"./drupal.behaviors.navigation_panel":6,"./drupal.behaviors.r4d.js":7,"./modal.js":9,"./references.js":14,"./vital-signs.js":15,"jquery-match-height":"jquery-match-height"}],9:[function(require,module,exports){
"use strict";

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// open the modal
(0, _jquery2.default)(".js-modal-trigger").click(function () {
  var selector = (0, _jquery2.default)(this).data("modal");
  var indicator_id = (0, _jquery2.default)(this).data("indicator-id");

  if ((0, _jquery2.default)(selector).length > 0) {
    (0, _jquery2.default)(selector).addClass("is-open");
    (0, _jquery2.default)("body").addClass("modal-open");
    (0, _jquery2.default)("[data-button-indicator-id='" + indicator_id + "']").click();
    // wait for animation
    setTimeout(function () {
      (0, _jquery2.default)("button.js-modal-close").first().focus();
    }, 300);
  } else {
    console.warn(selector + " not found!");
  }
});

// close the modal
(0, _jquery2.default)(".js-modal-close").click(function () {
  var modal = (0, _jquery2.default)(this).closest(".modal");
  modal.removeClass("is-open");
  (0, _jquery2.default)("body").removeClass("modal-open");
});

if (document.querySelectorAll(".js-modal-scroll-open").length) {
  window.addEventListener('scroll', showModal, false);
}

function showModal(e) {
  if (window.scrollY > 10) {
    window.removeEventListener('scroll', showModal, false);
    setTimeout(function () {
      document.querySelector(".js-modal-scroll-open").classList.remove("js-modal-scroll-open");
    }, 4000);
  }
}

},{"jquery":"jquery"}],10:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var VizVitalsBarPhc = function (_Component) {
  _inherits(VizVitalsBarPhc, _Component);

  function VizVitalsBarPhc(props) {
    _classCallCheck(this, VizVitalsBarPhc);

    var _this = _possibleConstructorReturn(this, (VizVitalsBarPhc.__proto__ || Object.getPrototypeOf(VizVitalsBarPhc)).call(this, props));

    _this.state = {
      value: 0,
      maxValue: 100
    };
    return _this;
  }

  _createClass(VizVitalsBarPhc, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setState(_extends({}, this.props));
    }
  }, {
    key: "render",
    value: function render() {
      var _state = this.state,
          value = _state.value,
          maxValue = _state.maxValue;


      var valueStyles = {
        width: value / maxValue * 100 + "%"
      };

      return _react2.default.createElement(
        "div",
        { className: "viz-vitals-bar-phc", "aria-label": value + "% on PHC" },
        _react2.default.createElement(
          "div",
          { className: "viz-vitals-bar-phc__value", style: valueStyles },
          _react2.default.createElement(
            "span",
            { className: "viz-vitals-bar-phc__display-value" },
            value
          ),
          _react2.default.createElement(
            "span",
            { className: "viz-vitals-bar-phc__label" },
            "% on PHC"
          )
        )
      );
    }
  }]);

  return VizVitalsBarPhc;
}(_react.Component);

VizVitalsBarPhc.propTypes = {
  value: _propTypes.number.isRequired,
  maxValue: _propTypes.number.isRequired
};
exports.default = VizVitalsBarPhc;

},{"prop-types":"prop-types","react":"react"}],11:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var VizVitalsBar = function (_Component) {
  _inherits(VizVitalsBar, _Component);

  function VizVitalsBar(props) {
    _classCallCheck(this, VizVitalsBar);

    var _this = _possibleConstructorReturn(this, (VizVitalsBar.__proto__ || Object.getPrototypeOf(VizVitalsBar)).call(this, props));

    _this.state = {
      displayValue: "0",
      value: 0,
      maxValue: 100,
      themeColor: "blank"
    };
    return _this;
  }

  _createClass(VizVitalsBar, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setState(_extends({}, this.props));
    }
  }, {
    key: "render",
    value: function render() {
      var _state = this.state,
          themeColor = _state.themeColor,
          value = _state.value,
          maxValue = _state.maxValue,
          displayValue = _state.displayValue;


      var themeClass = themeColor ? "viz-vitals-bar__value--" + themeColor : "";

      var valueStyles = {
        minWidth: value / maxValue * 100 + "%"
      };

      return _react2.default.createElement(
        "div",
        { className: "viz-vitals-bar", "aria-label": displayValue + "%" },
        _react2.default.createElement(
          "div",
          { className: "viz-vitals-bar__label viz-vitals-bar__label--min" },
          "0"
        ),
        _react2.default.createElement(
          "div",
          { className: "viz-vitals-bar__label viz-vitals-bar__label--mid" },
          maxValue / 2
        ),
        _react2.default.createElement(
          "div",
          { className: "viz-vitals-bar__label viz-vitals-bar__label--max" },
          maxValue
        ),
        _react2.default.createElement(
          "div",
          { className: "viz-vitals-bar__value " + themeClass, style: valueStyles },
          displayValue
        )
      );
    }
  }]);

  return VizVitalsBar;
}(_react.Component);

VizVitalsBar.propTypes = {
  displayValue: _propTypes.string.isRequired,
  value: _propTypes.number.isRequired,
  maxValue: _propTypes.number.isRequired,
  themeColor: _propTypes.string
};
exports.default = VizVitalsBar;

},{"prop-types":"prop-types","react":"react"}],12:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var VizVitalsBlock = function (_Component) {
  _inherits(VizVitalsBlock, _Component);

  function VizVitalsBlock(props) {
    _classCallCheck(this, VizVitalsBlock);

    var _this = _possibleConstructorReturn(this, (VizVitalsBlock.__proto__ || Object.getPrototypeOf(VizVitalsBlock)).call(this, props));

    _this.state = {
      value: 0,
      themeColor: ""
    };
    return _this;
  }

  _createClass(VizVitalsBlock, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setState(_extends({}, this.props));
    }

    /** takes in a value 1-4 and color
    * returns a array with four items
    */

  }, {
    key: "renderChart",
    value: function renderChart(value, color) {
      return Array.apply(null, Array(4)).map(function (e, i) {
        var themeClass = i < Math.floor(value) ? "viz-vitals-block__chart--" + color : "";
        return _react2.default.createElement("div", { key: i, className: "viz-vitals-block__chart " + themeClass });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _state = this.state,
          themeColor = _state.themeColor,
          value = _state.value;
      var displayValue = this.props.displayValue;


      return _react2.default.createElement(
        "div",
        { className: "viz-vitals-block" },
        _react2.default.createElement(
          "div",
          { className: "viz-vitals-block__value" },
          displayValue
        ),
        this.renderChart(value, themeColor)
      );
    }
  }]);

  return VizVitalsBlock;
}(_react.Component);

VizVitalsBlock.propTypes = {
  value: _propTypes.number.isRequired,
  displayValue: _propTypes.string.isRequired,
  themeColor: _propTypes.string.isRequired
};
exports.default = VizVitalsBlock;

},{"prop-types":"prop-types","react":"react"}],13:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var VizVitalsRange = function (_Component) {
  _inherits(VizVitalsRange, _Component);

  function VizVitalsRange(props) {
    _classCallCheck(this, VizVitalsRange);

    var _this = _possibleConstructorReturn(this, (VizVitalsRange.__proto__ || Object.getPrototypeOf(VizVitalsRange)).call(this, props));

    var _this$props = _this.props,
        maxValue = _this$props.maxValue,
        lowValueLabel = _this$props.lowValueLabel,
        highValueLabel = _this$props.highValueLabel;


    _this.state = {
      lowValue: 0,
      highValue: maxValue,
      lowDisplayValue: "0",
      highDisplayValue: "" + maxValue,
      lowValueLabel: lowValueLabel || "",
      highValueLabel: highValueLabel || "",
      maxValue: maxValue,
      themeColor: ""
    };
    return _this;
  }

  _createClass(VizVitalsRange, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _props = this.props,
          maxValue = _props.maxValue,
          highValue = _props.highValue;

      // make sure the range includes the max value

      var max = maxValue < highValue ? Math.round(highValue / 50) * 50 : maxValue;

      this.setState(_extends({}, this.props, { maxValue: max }));
    }
  }, {
    key: "render",
    value: function render() {
      var _state = this.state,
          themeColor = _state.themeColor,
          lowValue = _state.lowValue,
          highValue = _state.highValue,
          lowDisplayValue = _state.lowDisplayValue,
          highDisplayValue = _state.highDisplayValue,
          lowValueLabel = _state.lowValueLabel,
          highValueLabel = _state.highValueLabel,
          maxValue = _state.maxValue;


      var themeClass = themeColor ? "viz-vitals-range--" + themeColor : "";

      var lowPos = lowValue / maxValue * 100;
      var maxPos = highValue / maxValue * 100;

      var valueStyles = {
        left: lowPos + "%",
        right: 100 - maxPos + "%"
      };

      var rangeClass = maxPos - lowPos < 9 ? "viz-vitals-range--narrow" : "";

      return _react2.default.createElement(
        "div",
        { className: "viz-vitals-range " + themeClass + " " + rangeClass },
        _react2.default.createElement(
          "div",
          { className: "viz-vitals-range__bar" },
          _react2.default.createElement(
            "div",
            { className: "viz-vitals-range__label viz-vitals-range__label--min" },
            "0"
          ),
          _react2.default.createElement(
            "div",
            { className: "viz-vitals-range__label viz-vitals-range__label--max" },
            maxValue
          )
        ),
        _react2.default.createElement(
          "div",
          { className: "viz-vitals-range__values", style: valueStyles },
          _react2.default.createElement("div", { className: "viz-vitals-range__range-bar viz-vitals-range__range-bar--bottom" }),
          _react2.default.createElement(
            "div",
            { className: "viz-vitals-range__handle-low" },
            _react2.default.createElement(
              "span",
              { className: "viz-vitals-range__low-value", "aria-label": "{ lowValueLabel }" },
              lowDisplayValue
            ),
            _react2.default.createElement(
              "span",
              { className: "viz-vitals-range__low-label" },
              lowValueLabel
            )
          ),
          _react2.default.createElement(
            "div",
            {
              className: "viz-vitals-range__handle-high" },
            _react2.default.createElement(
              "span",
              { className: "viz-vitals-range__high-value", "aria-label": "{ highValueLabel }" },
              highDisplayValue
            ),
            _react2.default.createElement(
              "span",
              { className: "viz-vitals-range__high-label" },
              highValueLabel
            )
          ),
          _react2.default.createElement("div", { className: "viz-vitals-range__range-bar viz-vitals-range__range-bar--top" })
        )
      );
    }
  }]);

  return VizVitalsRange;
}(_react.Component);

VizVitalsRange.propTypes = {
  lowValue: _propTypes.number.isRequired,
  highValue: _propTypes.number.isRequired,
  lowDisplayValue: _propTypes.string.isRequired,
  highDisplayValue: _propTypes.string.isRequired,
  lowValueLabel: _propTypes.string,
  highValueLabel: _propTypes.string,
  maxValue: _propTypes.number.isRequired,
  themeColor: _propTypes.string.isRequired
};
exports.default = VizVitalsRange;

},{"prop-types":"prop-types","react":"react"}],14:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  var window = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window;
  var document = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;


  if ((0, _jquery2.default)(".js-indicator-reference").length && (0, _jquery2.default)(".js-references-target").length) {
    (0, _jquery2.default)(".js-indicator-reference").each(function (i) {
      (0, _jquery2.default)(this).text(i + 1);
      (0, _jquery2.default)(".js-references-target").append("<li>" + (0, _jquery2.default)(this).data("reference") + "</li>");
    });
    (0, _jquery2.default)(".js-references-target").parent().find("h2").show();
  }
}(window, document);

},{"jquery":"jquery"}],15:[function(require,module,exports){
"use strict";

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactDom = require("react-dom");

var _d = require("d3");

var d3 = _interopRequireWildcard(_d);

var _vizVitalsRange = require("./react/viz-vitals-range.jsx");

var _vizVitalsRange2 = _interopRequireDefault(_vizVitalsRange);

var _vizVitalsBlock = require("./react/viz-vitals-block.jsx");

var _vizVitalsBlock2 = _interopRequireDefault(_vizVitalsBlock);

var _vizVitalsBar = require("./react/viz-vitals-bar.jsx");

var _vizVitalsBar2 = _interopRequireDefault(_vizVitalsBar);

var _vizVitalsBarPhc = require("./react/viz-vitals-bar-phc.jsx");

var _vizVitalsBarPhc2 = _interopRequireDefault(_vizVitalsBarPhc);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var convert2Number = function convert2Number(value) {
  return Math.round(parseFloat(value));
};

// forEach polyFilled for IE11
document.querySelectorAll(".js-vital-sign-range").forEach(function (el) {

  var props = {
    lowValue: convert2Number(el.getAttribute("data-low") || 0),
    highValue: convert2Number(el.getAttribute("data-high") || 0),
    lowDisplayValue: el.getAttribute("data-display-low") || "0",
    highDisplayValue: el.getAttribute("data-display-high") || "0",
    lowValueLabel: el.getAttribute("data-low-label"),
    highValueLabel: el.getAttribute("data-high-label"),
    maxValue: parseInt(el.getAttribute("data-max") || 100),
    themeColor: el.getAttribute("data-color")
  };

  (0, _reactDom.render)(_react2.default.createElement(_vizVitalsRange2.default, props), el);
});

// forEach polyFilled for IE11
document.querySelectorAll(".js-vital-sign-block").forEach(function (el) {
  var d = parseFloat(el.getAttribute("data-value") || 0);
  var displayValue = el.getAttribute("data-simple-value");
  var min = 0;
  var max = 4;

  // value is a range 0 -> 4
  var value = d < min ? min : d > max ? max : d;

  var props = {
    value: value,
    displayValue: displayValue,
    themeColor: el.getAttribute("data-color")
  };

  (0, _reactDom.render)(_react2.default.createElement(_vizVitalsBlock2.default, props), el);
});

// forEach polyFilled for IE11
document.querySelectorAll(".js-vital-sign-bar").forEach(function (el) {

  var props = {
    displayValue: el.getAttribute("data-display-value") || "0",
    value: parseFloat(el.getAttribute("data-value") || 0),
    maxValue: parseInt(el.getAttribute("data-max") || 100),
    themeColor: el.getAttribute("data-color") || null
  };

  if (el.getAttribute("data-type").toLowerCase() === "percent") {
    props.value = convert2Number(props.value * 100);
  } else {
    props.value = convert2Number(props.value);
  }

  (0, _reactDom.render)(_react2.default.createElement(_vizVitalsBar2.default, props), el);
});

// forEach polyFilled for IE11
document.querySelectorAll(".js-vital-sign-bar-phc").forEach(function (el) {

  var props = {
    value: convert2Number(el.getAttribute("data-value") || 0),
    maxValue: parseInt(el.getAttribute("data-max") || 100)
  };

  (0, _reactDom.render)(_react2.default.createElement(_vizVitalsBarPhc2.default, props), el);
});

// forEach polyFilled for IE11
document.querySelectorAll(".js-vital-sign-donut").forEach(function (el) {
  var value = convert2Number(el.getAttribute("data-value") || 0);
  var selector = d3.select(el);

  createDonut(selector, value);
});

function createDonut(selector, value) {
  var data = [100 - value, value];
  var height = 120;
  var width = 120;
  var radius = width / 2;
  var color = d3.scaleOrdinal(['#28265f', '#8988a3']);

  var pie = d3.pie().value(function (d) {
    return d;
  }).sort(null);
  var path = d3.arc().innerRadius(32).outerRadius(radius);

  var svg = selector.append("svg").attr("width", width).attr("height", height).attr("class", 'viz-vitals-donut__svg');

  var g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

  g.selectAll(".arc").data(pie(data)).enter().append("g").attr("class", "viz-vitals-donut__arc").append("path").attr("d", path).attr("fill", function (d) {
    return color(d.data);
  });
}

},{"./react/viz-vitals-bar-phc.jsx":10,"./react/viz-vitals-bar.jsx":11,"./react/viz-vitals-block.jsx":12,"./react/viz-vitals-range.jsx":13,"d3":"d3","react":"react","react-dom":"react-dom"}]},{},[8])

//# sourceMappingURL=global-generated.js.map
