(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

(function (ElementProto) {
  if (typeof ElementProto.matches !== "function") {
    ElementProto.matches = ElementProto.msMatchesSelector || ElementProto.mozMatchesSelector || ElementProto.webkitMatchesSelector || function matches(selector) {
      var element = this;
      var elements = (element.document || element.ownerDocument).querySelectorAll(selector);
      var index = 0;

      while (elements[index] && elements[index] !== element) {
        ++index;
      }

      return Boolean(elements[index]);
    };
  }

  if (typeof ElementProto.closest !== "function") {
    ElementProto.closest = function closest(selector) {
      var element = this;

      while (element && element.nodeType === 1) {
        if (element.matches(selector)) {
          return element;
        }

        element = element.parentNode;
      }

      return null;
    };
  }
})(window.Element.prototype);

},{}],2:[function(require,module,exports){
"use strict";

require("core-js/fn/object/assign.js");

require("core-js/fn/array/find.js");

require("core-js/fn/array/find-index.js");

require("core-js/fn/array/fill.js");

require("core-js/fn/array/includes.js");

require("core-js/fn/array/from.js");

require("./closest.js");

require("./nodelist-foreach.js");

require("core-js/es6/map.js");

require("core-js/es6/set.js");

require("whatwg-fetch");

var _promisePolyfill = require("promise-polyfill");

var _promisePolyfill2 = _interopRequireDefault(_promisePolyfill);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// window.fetch and window.Promise


// for react 16
// https://facebook.github.io/react/blog/2017/09/26/react-v16.0.html#javascript-environment-requirements
if (!window.Promise) {
  window.Promise = _promisePolyfill2.default;
}

},{"./closest.js":1,"./nodelist-foreach.js":3,"core-js/es6/map.js":"core-js/es6/map.js","core-js/es6/set.js":"core-js/es6/set.js","core-js/fn/array/fill.js":"core-js/fn/array/fill.js","core-js/fn/array/find-index.js":"core-js/fn/array/find-index.js","core-js/fn/array/find.js":"core-js/fn/array/find.js","core-js/fn/array/from.js":"core-js/fn/array/from.js","core-js/fn/array/includes.js":"core-js/fn/array/includes.js","core-js/fn/object/assign.js":"core-js/fn/object/assign.js","promise-polyfill":"promise-polyfill","whatwg-fetch":"whatwg-fetch"}],3:[function(require,module,exports){
"use strict";

if (window.NodeList && !NodeList.prototype.forEach) {
  NodeList.prototype.forEach = function (callback, thisArg) {
    thisArg = thisArg || window;
    for (var i = 0; i < this.length; i++) {
      callback.call(thisArg, this[i], i, this);
    }
  };
}

},{}]},{},[2])

//# sourceMappingURL=polyfill-generated.js.map
