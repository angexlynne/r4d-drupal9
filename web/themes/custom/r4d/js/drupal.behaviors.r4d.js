/* global Drupal */
import $ from "jquery";
import enquire from "enquire.js";
import "jquery-match-height";

Drupal.behaviors.r4d = {
  attach: function (context, settings) {
    if($(".node-type-homepage").length) {
      $("#header .close").click(function(){
        $("#header").slideUp("fast");
      });
    }
    $("input, select, textarea").on("focus blur", function(event) {
      $("meta[name=viewport]").attr("content", "width=device-width,initial-scale=1,maximum-scale=" + (event.type == "blur" ? 10 : 1));
    });
    $(window).load(function() {
      if ($(".js-compare-table__pagination-next").length &&
      $(".js-compare-table__pagination-prev").length) {
        $(".js-compare-table__pagination-prev").hover(
          function() {
            $(this).parent().addClass("compare-table__pagination--hover");
          },
          function() {
            $(this).parent().removeClass("compare-table__pagination--hover");
          }
        );
        $(".js-compare-table__pagination-next").hover(
          function() {
            $(this).parent().addClass("compare-table__pagination--hover");
          },
          function() {
            $(this).parent().removeClass("compare-table__pagination--hover");
          }
        );
      }
    });

    $(".image_pull--center").each(function(){
      $(this).wrap('<div class="image_pull--center_wrapper"></div>');
    });

    $(".centered_content_image_890").each(function(){
      $(this).wrap('<div class="image_pull--center_wrapper"></div>');
    });

    $(".hero_content_image_1200").each(function(){
      $(this).wrap('<div class="image_pull--center_wrapper"></div>');
    });

    //Article Content Body - add class when right rail exists
    $(".right_rail").prev(".content_body").addClass("with-right-rail");
    $(".right_rail").siblings(".related_tags").addClass("with-right-rail");
    $(".right_rail").parents("#content").addClass("with-right-rail");

    // Show / Hide
    $(".js-accordion-click").click(function(){
      $(".js-accordion-target").slideToggle("slow");
      $(".masthead_tag--morelink").hide();
    });

    $(".js-accordion-click2").click(function(){
      $(".js-accordion-target").slideToggle("slow");
      $(".masthead_tag--morelink").show();
    });

    $(document).ajaxSend(function(e, xhr, settings) {
      if (settings.extraData._triggering_element_name == "newsletter-submit") {
        $("form#r4d-signup-mailchimp-form").fadeTo("150", ".5");
      }
    }).ajaxComplete(function(e, xhr, settings) {
      if (settings.extraData._triggering_element_name == "newsletter-submit") {
        $("form#r4d-signup-mailchimp-form").fadeTo("150", "1");
      }
    });
    // unbind all events attached to document
    $(document).off();

    /** vital stats responsiveness **/
    if($(".vital-stats__list").length && !($("html").not("#print-preview"))) {
      var $statFeatured = $(".js-featured-vital-stats"),
        $statHidden = $(".js-hidden-vital-stats");

      enquire
        .register("screen and (max-width: 990px)", {
          match: function() {
            $statFeatured.find(".vital-stats__item:nth-of-type(3)").prependTo($statHidden);
            var $parentElement = $statHidden.find(".vital-stats__item:nth-of-type(1)"),
              $orderElement = $parentElement.find(".vital-stats__value");

            $orderElement.prependTo($parentElement);
          },
          unmatch: function() {
            $statHidden.find(".vital-stats__item:nth-of-type(1)").appendTo($statFeatured);
            var $parentElement = $statFeatured.find(".vital-stats__item:nth-of-type(3)"),
              $orderElement = $parentElement.find(".vital-stats__value");
            $orderElement.appendTo($parentElement);
          }
        })
        .register("screen and (max-width: 640px)", {
          match: function() {
            $statFeatured.find(".vital-stats__item:nth-of-type(2)").prependTo($statHidden);
            var $parentElement = $statHidden.find(".vital-stats__item:nth-of-type(1)"),
              $orderElement = $parentElement.find(".vital-stats__value");

            $orderElement.prependTo($parentElement);
          },
          unmatch: function() {
            $statHidden.find(".vital-stats__item:nth-of-type(1)").appendTo($statFeatured);
            var $parentElement = $statFeatured.find(".vital-stats__item:nth-of-type(2)"),
              $orderElement = $parentElement.find(".vital-stats__value");
            $orderElement.appendTo($parentElement);
          }
        })
        .register("screen and (max-width: 540px)", {
          match: function() {
            $statHidden.find(".vital-stats__item").each(function(){
              var $currentItem = $(this),
                $orderElement = $currentItem.find(".vital-stats__value");
              $orderElement.appendTo($currentItem);
            });
          },
          unmatch: function() {
            $statHidden.find(".vital-stats__item").each(function(){
              var $currentItem = $(this),
                $orderElement = $currentItem.find(".vital-stats__value");
              $orderElement.prependTo($currentItem);
            });
          }
        });
    }

    //set classes on HTML based on media query
    enquire
      .register("screen and (min-width: 1081px)", {
        match: function() {
          if($("html").hasClass("hamburger-menu-active")) {
            $(".js-responsive-nav-trigger").click();
          }
          $("html").addClass("view-desktop-nav");
        },
        unmatch: function() {
          $("html").removeClass("view-desktop-nav");
        }
      })
      .register("screen and (min-width: 801px)", {
        match: function() {
          $("html").addClass("view-desktop");
        },
        unmatch: function() {
          $("html").removeClass("view-desktop");
        }
      });

    /** Add Padding top to body tag when user is logged in **/
    bodyTag();
    $(window).resize(function() {
      bodyTag();
    });
    function bodyTag() {
      var $toolBarMenu = $(".toolbar-menu"),
        toolBarMenuHeight = "";
      if($toolBarMenu.length) {
        toolBarMenuHeight = $toolBarMenu.outerHeight();
        $("body").css("padding-top", toolBarMenuHeight);
      }
    }

    /** Responsive Nav **/
    responsiveNav();
    function responsiveNav() {

      var $linkClone = "",
        $prependCloneTo = "",
        navDrawerTriggerClass = "js-nav_drawer-trigger",
        navDrawerTargetClass = "js-nav_drawer--target";

      //clone and prepend landing page link to dropdown menu
      $(".js-has-children").each(function(){
        var $link = $(this).find(".site-nav__item-link");
        if($link.attr("href") === "" || $link.attr("href") === "#") {
          return;
        } else {
          $linkClone = $link.clone().removeAttr("class").addClass("site-subnav__item-link");
          $prependCloneTo = $(this).find(".site-subnav");
          $linkClone.wrap('<li class="site-subnav__item cloned"></li>').parent().prependTo($prependCloneTo);
        }
      });

      //insert selected nav_drawer to it's respective link triggers when window is resized
      $("." + navDrawerTriggerClass).each(function(){
        if($(this).is(".selected")) {
          $("." + navDrawerTargetClass).insertAfter($(this));
        }
      });
      //insert nav_drawer to it's respective link triggers on click
      $(document).on("click", "." + navDrawerTriggerClass, function() {
        $("." + navDrawerTargetClass).insertAfter($(this));
      });

      var navTriggerClass = "js-responsive-nav-trigger",
        subnavTriggerClass = "js-responsive-subnav-trigger";

      $(document).on("click", "." + navTriggerClass, function() {
        $("html").toggleClass("hamburger-menu-active");
        $(this).toggleClass("is-active");
        $(window).trigger("resize");
      });

      $(document).on("click", "." + subnavTriggerClass, function() {
        $(this).toggleClass("is-active");
        return false;
      });
    }

    /** Sticky Newsletter section **/
    // stickyNewsletter();
    // $(window).scroll(function () {
    //   stickyNewsletter();
    // });
    // function stickyNewsletter() {
    //   var stickyElementClass = "js-newsletter-sticky",
    //     scrollTop = $(window).scrollTop(),
    //     documentHeight = $(document).height(),
    //     windowHeight = $(window).height(),
    //     scrollBottom = parseInt(windowHeight) + parseInt(scrollTop),
    //     stickyIntersection = documentHeight - $(".js-site-footer").outerHeight();

    //   enquire.register("screen and (min-width: 801px)", {
    //     match: function() {
    //       if(stickyIntersection >= scrollBottom) {
    //         $("." + stickyElementClass).addClass("am-stuck");
    //       } else {
    //         $("." + stickyElementClass).removeClass("am-stuck");
    //       }
    //     },
    //     unmatch: function() {
    //       $("." + stickyElementClass).removeClass("am-stuck");
    //     }
    //   });
    // }

    /** Sticky Compare Legend **/
    //stickyLegend();
    $(window).on("load scroll resize", function () {
      stickyLegend();
    });

    function stickyLegend() {
      var stickyWithinClass = "",
        stickyLegendClass = "";

      if($("body").hasClass("node-type-indicator")) {
        stickyWithinClass = "js-indicator--chart";
        stickyLegendClass = "js-indicator--chart__legend";
      }else if($("body").hasClass("node-type-comparison")) {
        stickyWithinClass = "js-compare-chart";
        stickyLegendClass = "js-compare-chart--legend";

      }
      else{
        return;
      }

      var stickyLegendTriggerClass = "js-legend-mobile--trigger",
        scrollTop = $(window).scrollTop(),
        stickyWithinOffset = $("." + stickyWithinClass).offset(),
        stickyStartAt = stickyWithinOffset.top,
        stickyEndAt = stickyStartAt + parseInt($("." + stickyWithinClass).outerHeight());

      if(stickyStartAt <= scrollTop && stickyEndAt >= scrollTop + 185) {
        if($("body").hasClass("node-type-indicator")) {
          $("." + stickyLegendClass).css("top", (scrollTop - stickyStartAt + parseInt($(".indicator_sum-title-sticky-wrap").outerHeight())));
          $("." + stickyLegendTriggerClass).css("top", (scrollTop - stickyStartAt + 35 + parseInt($(".indicator_sum-title-sticky-wrap").outerHeight())));
        } else {
          $("." + stickyLegendClass).css("top", (scrollTop - stickyStartAt));
          $("." + stickyLegendTriggerClass).css("top", (scrollTop - stickyStartAt + 35));
        }
      } else {
        $("." + stickyLegendClass).css("top", "");
        $("." + stickyLegendTriggerClass).css("top", "");
      }
    }

    /** match height deep dive tiles **/
    $(".js-data_tile").matchHeight();

    if ($(".indicator_box").length && !$("body").hasClass("node-type-indicator-library")) {
      $(".indicator_box").matchHeight();
    }
  }
};
