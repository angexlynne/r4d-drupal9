(function($){
$(document).ready(init);
function init() {	
	mobile();
}

var clickFunction = "click";

function isTouchDevice() {
    return 'ontouchstart' in document.documentElement;
}

function mobile() {
	if (matchMedia) {
	  var mq = window.matchMedia("(min-width: 769px)");
	  mq.addListener(WidthChange);
	  WidthChange(mq);
	}

	// media query change
	function WidthChange(mq) {
	  if (mq.matches) {
	    heroIntro();
	    
	    $(".home__hero--bubbles .home__hero--bubble").removeAttr("style").removeClass("hero-mobile");
	  } else {
		randomMobile();
		$(".home__hero--bubbles .home__hero--bubble").addClass("hero-active hero-end hero-mobile");
	  }

	}

	if (isTouchDevice()) {
    	// on Mobile
    	heroClick();
	}
	else {
    	// on Desktop
     	heroHover();
	}
}

function heroIntro() {

	$(".home__hero--bubbles .home__hero--bubble").each(function(i){
		var that = $(this);
		var bubbleNum = i + 1;
		var time = 175;
		if ( !$(this).hasClass("hero-mobile") ) {
			
			setTimeout(function(){
				that.addClass("hero-active");
			}, (bubbleNum * time) );	

		}

	});


}

function heroHover() {
	$(".home__hero .home__hero--bubble.hero-stat")
		.mouseover(function(){
			if ( !$(this).hasClass("hero-mobile") ) {
				$(".home__hero .home__hero--bubble").not(this).addClass("hero-dim");
				$(this).addClass("hero-focus");
			}
		})
		.mouseout(function(){
			if ( !$(this).hasClass("hero-mobile") ) {
				$(".home__hero .home__hero--bubble").removeClass("hero-dim hero-focus");
			}
		});
}

function heroClick() {
	$(".home__hero .home__hero--bubble.hero-stat").on("click", function(){
		if ( !$(this).hasClass("hero-mobile") ) {
			if ( !$(this).hasClass("hero-focus") ) {
				$(".home__hero .home__hero--bubble").not(this).removeClass("hero-focus").addClass("hero-dim");
				$(this).addClass("hero-focus").removeClass("hero-dim");
			} else {
				$(".home__hero .home__hero--bubble").removeClass("hero-focus hero-dim");
			}
		} 
	});
}

function randomMobile() {
	var whichMessage = Math.floor(Math.random() * 9) + 1;

	$(".home__hero--bubble.hero-mobile" + whichMessage).show();

}
})(jQuery);