/**
 * @file
 * Swbook_user js statistics.
 */
 (function ($, Drupal) {

  'use strict';

  Drupal.behaviors.navigation_panel = {
    attach: function (context, settings) {

      $('.menu-improvement-strategies > li > a').on('click', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('open');
        $(this).parent().find('.menu.accordion-items').toggleClass('open');
      });

      $('#navigation-panel').on('click', function(e) {
        if ($(window).width() < 640) {
          if ($('#navigation-panel').hasClass('open') && $(e.target).prop("id") == 'navigation-panel') {
            $(this).removeClass('open');
          }
          else if ($(e.target).prop("tagName") != 'A') {
            $(this).addClass('open');
          }
        }
      });

    }
  }
})(jQuery, Drupal);
