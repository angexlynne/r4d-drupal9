var PHCPI = PHCPI || {};

PHCPI.indicatorSlider = (function($) {
	'use strict';

	var self = {};
	var debug = false;

	var defaultSliderClass = 'js-bxslider',
		mediumSliderClass = 'js-bxslider-tablet',
		mediumBreakpoint = 750, // px, tablet
		xsmallBreakpoint = 630, // mobile
		collapseOpenClass = 'collapse-open',
		collapseBodyClass = 'accordion_body',
		sliders = [], // nested arrays of sliders based on accordion category
		/** @type {Object} list of flags which indicate which sliders have
							been initialized */
		slidersSet = {
			medium: false,
			xsmall: false
		},
		resizeTimeout;

	/** @type {Object} Enumeration for which flags to set */
	var flagToSet = {
		'Xsmall' : 0,
		'Medium' : 1
	};

	/**
	 * Initialize sliders if they are in an open accordion and the slider does
	 * not already exist. Make sure to call hasReachedBreakpoint before
	 * calling createSliders.
	 * @param  {Integer} breakpoint	The breakpoint at which the slider will
	 *                              initialize
	 * @param  {String} sliderClass	The html class that signals a slider
	 *                              needs to be initalized
	 * @return
	 */
	self.createSliders = function(sliderClass) {
		if (debug) {
			console.log("createSliders() called");
		}

		var sliderReloaded; // which slider (xsmall or medium) has been reloaded

		// only initialize sliders in open accordions
		$.each($('.' + collapseOpenClass), function(index, value) {
			var collapseContainerIndex = $(this).parent().index(),
				$sliderElements = 	$(this)
									.siblings('.' + collapseBodyClass)
									.find('.' + sliderClass);

			if (!sliders[collapseContainerIndex].length) { // sliders not initialized
				if (debug) {
					console.log("Number of sliders in collapse container "
						+ collapseContainerIndex + ": "
						+ $sliderElements.length);
				}

				if (self.hasReachedBreakpoint(xsmallBreakpoint)) {
					// initialize sliders
					$.each($sliderElements, function() {
						sliders[collapseContainerIndex].push($(this).bxSlider({
							minSlides: 1,
							maxSlides: 1,
							moveSlides: 1,
							pager: false,
							slideWidth: 320,
							slideSelector: 'div',
							infiniteLoop: false,
							controls: true,
							hideControlOnEnd: true
						}));
					});

					slidersSet.xsmall = true;
				}
				else if (self.hasReachedBreakpoint(mediumBreakpoint)) {
					// initialize sliders
					$.each($sliderElements, function() {
						sliders[collapseContainerIndex].push($(this).bxSlider({
							minSlides: 2,
							maxSlides: 2,
							moveSlides: 1,
							pager: false,
							slideWidth: 265,
							slideSelector: 'div',
							infiniteLoop: false,
							controls: true,
							hideControlOnEnd: true
						}));
					});

					slidersSet.medium = true;
				}

				if (debug) {
					console.log(sliders[collapseContainerIndex]);
				}
			} else { // sliders have been initialized
				// Check if the correct sliders have been initialized
				if(debug) {
					console.log("Sliders in collapse container: " +
						collapseContainerIndex +
						" have already been initialized.");
				}
				if (self.hasReachedBreakpoint(xsmallBreakpoint) && !slidersSet.xsmall) {
					if (debug) {
						console.log("Reloading xsmall sliders");
					}
					// reload xsmall sliders
					$.each(sliders[collapseContainerIndex], function() {
						this.reloadSlider({
							minSlides: 1,
							maxSlides: 1,
							moveSlides: 1,
							pager: false,
							slideWidth: 320,
							slideSelector: 'div',
							infiniteLoop: false,
							controls: true,
							hideControlOnEnd: true
						});
					});

					sliderReloaded = flagToSet.Xsmall;
				}
				// check if the medium breakpoint has been reached but
				// medium sliders not initialized
				else if (self.hasReachedBreakpoint(mediumBreakpoint) && !slidersSet.medium &&
					!self.hasReachedBreakpoint(xsmallBreakpoint)) {
					if (debug) {
						console.log("Reloading medium sliders");
					}
					// reload medium sliders
					$.each(sliders[collapseContainerIndex], function() {
						this.reloadSlider({
							minSlides: 2,
							maxSlides: 2,
							moveSlides: 1,
							pager: false,
							slideWidth: 265,
							slideSelector: 'div',
							infiniteLoop: false,
							controls: true,
							hideControlOnEnd: true
						});
					});

					sliderReloaded = flagToSet.Medium;
				}
			}
		});
		if (sliderReloaded == 0) { // xsmall
			// need to set xsmall flag to indicate that the xsmall breakpoint
			// has been reached and the sliders have been initialized
			// at this breakpoint
			if (debug) {
				console.log("Set xsmall flag");
			}
			slidersSet.xsmall = true;
			slidersSet.medium = false;
		}
		else if (sliderReloaded == 1) { // medium
			if (debug) {
				console.log("Set medium flag");
			}
			slidersSet.xsmall = false;
			slidersSet.medium = true;
		}
	}; // createSliders = function ()

	/**
	 * Checks to see if the window width is less than the given breakpoint
	 * @param  {Integer}  breakpoint	The breakpoint used to check against the
	 *                               	window width
	 * @return {Boolean}				Returns true if the window width is less
	 *                              	than the given breakpoint, false otherwise.
	 */
	self.hasReachedBreakpoint = function(breakpoint) {
		if (window.innerWidth <= breakpoint) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if there are sliders inside slider array. If there are, destroy them.
	 * @return
	 */
	self.destroySliders = function() {
		for (var i = 0; i < sliders.length; i++) {
			if (sliders[i].length > 0) {
				if (debug) {
					console.log("Destroying sliders. Slider container index: " + i);
				}
				// Destroy all sliders
				for (var j = (sliders[i].length - 1); j >= 0; j--) {
					// start with the last item of the first array because
					// splicing the element will change the remaining item(s) indexing
					sliders[i][j].destroySlider();
					sliders[i].splice(j, 1);
					if (debug) {
						console.log(sliders[i]);
					}
				}
			}
		}

		// reset flags
		slidersSet.xsmall = false;
		slidersSet.medium = false;
	}

	/**
	 * If the window has been resized, check to see if it has reached a breakpoint
	 * If it has, create appropriate sliders
	 * If it has not, destroy any appropriate sliders
	 * @param  {Integer} windowWidth	The current window width
	 * @param  {String} breakpointClass	The class used to target the slider
	 * @return {}
	 */
	self.windowResized = function(windowWidth, breakpointClass) {
		if (self.hasReachedBreakpoint(xsmallBreakpoint) ||
			self.hasReachedBreakpoint(mediumBreakpoint)) {
			self.createSliders(breakpointClass);
		} else {
			self.destroySliders();
		}
	}

	self.collapseOpened = function() {
		// When a collapsible container has opened, check the page width
		// then check to see if a sliderSet flag has been set
		// do proper intializations / slider reloads
		if (debug) {
			console.log("collapseOpened() was called.");
		}

		if (self.hasReachedBreakpoint(xsmallBreakpoint) ||
			self.hasReachedBreakpoint(mediumBreakpoint)) {
			self.createSliders(mediumSliderClass);
		}
	}

	/**
	 * On page load initialize sliders and add window resize listeners
	 * @return nothing returned
	 */
	self.sliderInit = function() {
		if (("." + mediumSliderClass).length) { // if there is a slider
			// find number of collapsible containers in order to initialize
			// the array of sliders
			var numCollapsibleContainers =	$("." + mediumSliderClass)
											.parents('.collapsible_container')
											.siblings('.collapsible_container')
											.length;
			for (var i = 0; i < numCollapsibleContainers; i++) {
				sliders[i] = [];
			}

			// on page load check browser width
			if (self.hasReachedBreakpoint(xsmallBreakpoint) ||
				self.hasReachedBreakpoint(mediumBreakpoint)) {
				if (debug) {
					console.log("On page load, calling createSliders() at " +
						window.innerWidth + "px page width.");
				}
				self.createSliders(mediumSliderClass);
			}
			$(window).resize(function() {
				clearTimeout(resizeTimeout);
				resizeTimeout = setTimeout(function() {
					self.windowResized(window.innerWidth, mediumSliderClass)
				}, 100);
			});
		}
	}; // self.sliderInit = function()

	/**
	 * Initialize sliders
	 * @return
	 */
	self.initialize = function () {
		self.sliderInit();
	};

	return self;

})(jQuery);
