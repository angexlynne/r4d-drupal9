﻿/// <reference path="/js/vendor/jquery/jquery-1.11.2.min.js"/>

var PHCPI = PHCPI || {};

PHCPI.navDrawer = (function($) {
	'use strict';

	var self = {};

	var debug = false;

	var drawerClass = 'js-nav_drawer--target',
	    triggerClass = 'js-nav_drawer-trigger',
	    activeClass = 'selected',
	    drawerContentMenu = 'options_list--menu';

	// Slide open the correct drawer
	self.openDrawer = function (contentTarget) {
		// Ensure the drawer content holders are closed, then display
		// only the correct one
		$('.drawer_content--container').hide();
		// show the correct content
		$('.drawer_content--container.js-' + contentTarget).show();
		// slide open the drawer
		$('.' + drawerClass).slideDown();
	};

	// Close the drawers
	self.closeDrawer = function() {
		$('.' + drawerClass).slideUp(function() {
			// After the close animation is complete, remove
			// the active class from the active item in the nav
			self.removeActive();
			// And hide the drawer content
			$('.drawer_container').hide();
		});
	};

	// Set the indicated item to selected/active
	self.setActive = function (target) {

		if (target.hasClass(activeClass)) {
			// If the target is already acitve,
			// close the drawer
			self.closeDrawer();
		} else {
			// Otherwise, remove the active class from any
			// other open item, and add it to this new item,
			// then open the drawer
			self.removeActive();
			target.addClass(activeClass);
			self.openDrawer(target.data('drawer'));
		}
		
	};

	// Remove the active/selected state from all applicable items
	self.removeActive = function() {
		$('.' + triggerClass).removeClass(activeClass);
	};

	self.bindTargets = function() {

		$('.' + triggerClass).on('click', function(e) {
			e.preventDefault();

			self.setActive($(this));
		});

		if (debug) {
			console.log('PHCPI.navDrawer.bindTargets has been run.');
		}

	}; // self.bindTargets = function ()



	// Nav Drawer Options Selector
	self.initDrawerContent = function() {
		$('.drawer_content--container').each(function () {
			var selectedOption = $(this).find('.options_list--menu .selected'),
				selectedPanel = $('.js-' + selectedOption.data('optionpanel'));

			selectedPanel.show();

		});
	}; // self.initDrawerContent = function()

	self.bindOptionsSelections = function() {

		$('.' + drawerContentMenu).on('click', 'li', function () {
			var clickedOption = $(this),
			    selectedPanel = $('.js-' + clickedOption.data('optionpanel'));

			if (!clickedOption.hasClass(activeClass)) {
				// If the clicked option does not already have the active class
				// add the active class to the clicked items, and remove
				// it from any siblings that already had it
				clickedOption
					.addClass(activeClass)
					.siblings().removeClass(activeClass);

				// Show the associated panel of lists and hide any of it's siblings
				// that were previously showing
				selectedPanel
					.show()
					.siblings('.options_selection--options_list').hide();
			}
		});

		$('.' + drawerContentMenu + " li:first").click();
	}


	self.initialize = function () {
		if (debug) {
			console.log('PHCPI.navDrawer has been initialized');
		}

		self.bindTargets();
		self.bindOptionsSelections();
		self.initDrawerContent();
	};

	return self;

})(jQuery);
