var PHCPI = PHCPI || {};

PHCPI.tooltip = (function($) {
	'use strict';

	var self = {};

	var hoverClass = 'tooltip-hover', // element that user will hover over
	    tooltip = 'tooltip', // element that pops up on hover
	    container = 'article'; // element that contains the popup

	self.positionTooltip = function() {
		jQuery.each($('.' + hoverClass), function(index, value) {
			$(this).hover(function() {
				// center tooltip over hover element
				var halfHoverElementWidth = $(this).outerWidth() / 2;
				// Half width needed to correctly position down arrow on tooltip
        var $tooltip = $(this).siblings('.' + tooltip);
				var halfTooltipWidth = ($tooltip.outerWidth()) / 2;
				var xCoord = halfHoverElementWidth - halfTooltipWidth;
				var xContainerPos = $(container).offset().left;
				var yCoord = ($tooltip.height() + 11) * -1 ;
				$tooltip.css({"left": xCoord, "top": yCoord});
        var xTooltipPos = $tooltip.offset().left;
        if(xTooltipPos < xContainerPos) {
          xCoord = xCoord + (xContainerPos - xTooltipPos) + 20;
          $tooltip.css("left", xCoord + 'px');
          var $arrow = $tooltip.find(".tooltip-arrow:not(.done)");
          if($arrow.length > 0) {
            var xCoordArrow = $arrow.position().left - (xContainerPos - xTooltipPos) - 20;
            $arrow.addClass("done").css("left", xCoordArrow + 'px');
          }

        }
			});
		});
	}; // self.positionTooltip = function()

	self.initialize = function () {
		self.positionTooltip();
	};

	return self;

})(jQuery);
