var PHCPI = PHCPI || {};

PHCPI.collapsible = (function($) {
	'use strict';

	var self = {};
	var debug = false;

	var mediumSliderClass = "js-bxslider-tablet",
		mediumBreakpoint = 750, // px
		collapsibleSpeed = 300, // ms
		defaultOpenIDs = "accordion_default,accordion_1,accordion_2,accordion_3,accordion_4,accordion_5,accordion_6,accordion_7,accordion_8,accordion_9,accordion_10";


	self.collapsibleInit = function() {
		$('.collapsible').collapsible({
			defaultOpen: defaultOpenIDs,
			speed: collapsibleSpeed,
		  	cookieName: '',
			animateOpen: function (elem, opts) {
		        elem.next().slideDown(opts.speed);
		        PHCPI.indicatorSlider.collapseOpened();
		    },
		    animateClose: function (elem, opts) {
		        elem.next().slideUp(opts.speed);
		    }
		});
	}

	self.collapsibleComponent = function() {

		var classes = {
			container: 'js-collapsible--component',
			target: 'js-collapsible--target',
			trigger: 'js-collapsible--trigger',
			active: 'is-open'
		};

        if (debug) {
          console.log("container: ", $('.' + classes.container));
          console.log("target: ", $('.' + classes.target));
          console.log("trigger: ", $('.' + classes.target));
        }

		if ($('.' + classes.container).length) {

			$('.' + classes.container).on('click', '.' + classes.trigger, function() {

				if (debug) {
					console.log("Trigger for a collapsible Component has been clicked!");
				} // end if

				var parent = $(this).parents('.' + classes.container);

				parent.toggleClass(classes.active);
				parent.find('.' + classes.target).toggleClass(classes.active);

			});

		} // end if
	} // self.collapsibleCOmponent

	self.initialize = function () {
		self.collapsibleInit();
		self.collapsibleComponent();
	};

	return self;

})(jQuery);
