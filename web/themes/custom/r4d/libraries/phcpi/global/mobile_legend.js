
/* Compare Legend Scripting */


var PHCPI = PHCPI || {};

PHCPI.mobileLegend = (function($) {
    'use strict';

    var self = {};

    var classes = {
        "parent"  : "js-legend-mobile",
        "trigger" : "js-legend-mobile--trigger",
        "triggerInternal" : "mobile-legend--trigger--internal",
        "target"  : "js-legend-mobile--target",
        "onState" : "is-on"
    };

    // Bind click events for the tabs
    self.bindLegendTrigger = function() {
        $('.' + classes.parent).on('click', '.' + classes.trigger,function(){
            var $this = $(this),
                $parent = $this.parents('.' + classes.parent),
                $target = $parent.find('.' + classes.target);

            $target.toggleClass(classes.onState);
        });

        $('.' + classes.parent).on('click', '.' + classes.triggerInternal, function(){
            $('.' + classes.trigger).trigger('click');
        });
    }; // self.bindTabs

    self.initialize = function () {
        self.bindLegendTrigger();
    };

    return self;

})(jQuery);
