var PHCPI = PHCPI || {};

PHCPI.indicatorSticky = (function($) {
	'use strict';

	var self = {};
	var debug = false;

	self.stickyInit = function () {
		// Make sure the Sticky element exists on the page before
		// actually initilizing the scripting.
		if ($('.js-sticky').length) {

			/* Expect only one sticky element on page */
			var $targetSticky = $('.js-sticky'),
				$stickyElement = $('.js-sticky-item'),
				stickyActive = 'sticky-stuck',
				stickyElementHeight = $stickyElement.outerHeight(),
				targetStickyOffset = $targetSticky.offset(),
				stickyHeight = targetStickyOffset.top + $targetSticky.outerHeight();

			$stickyElement.css("top", -stickyElementHeight);

			$(window).scroll(function () {
				if ($stickyElement.hasClass(stickyActive)) {
					if ($(window).scrollTop() < stickyHeight) {
						$stickyElement.removeClass(stickyActive);
						$stickyElement.css("top", -stickyElementHeight);
					}
				}
				else if (!$stickyElement.hasClass(stickyActive)) {
					if ($(window).scrollTop() >= stickyHeight) {
						$stickyElement.addClass(stickyActive);
						$stickyElement.css("top", 0);
					}
				}
			});

		} // if ($('.js-sticky').length)

	}; // self.stickyInit = function()

	self.initialize = function () {
		self.stickyInit();
	};

	return self;

})(jQuery);
