/**
 * Check for CSS Blend Mode support
 * https://css-tricks.com/basics-css-blend-modes/
 */

var PHCPI = PHCPI || {};

PHCPI.cssBlendModeCheck = (function($) {
	'use strict';

	var self = {};
	var debug = false;

	self.checkInit = function() {
		if (window.getComputedStyle(document.body).backgroundBlendMode) {
			$('html').addClass('blendmode');
		} else {
			$('html').addClass('no-blendmode');
		}
	}

	self.initialize = function () {
		self.checkInit();
	};

	return self;

})(jQuery);