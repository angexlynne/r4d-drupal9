
var PHCPI = PHCPI || {};

PHCPI.tabGroups = (function($) {
    'use strict';

    var self = {};
    var debug = false;

    var classes = {
        "parent"  : "js-tab-groups",
        "trigger" : "js-tab-groups__trigger",
        "target"  : "js-tab-groups__target",
        "onState" : "is-on"
    };

    // Bind click events for the tabs
    self.bindTabs = function() {
        $('.' + classes.parent).on('click', '.' + classes.trigger,function(){
            var $this = $(this),
                $parent = $this.parents('.' + classes.parent),
                thisIndex = $this.index(),
                $targetPanel;

            if(debug) {
                console.log('A tab has been clicked!', $this);
                console.log('My index is: ', $this.index());
                console.log('My parent is: ', $parent);
            }

            if(!$this.hasClass('.' + classes.onState)) {
                // set the $targetPanel variable to what panel we want
                // to display before passing the variable into our function
                $targetPanel = $parent
                    .find('.' + classes.target + ':eq(' + thisIndex + ')');

                self.switchTab($this, $targetPanel);
            }

        });

        // if there is a data-tooltip on the element, add the element
        $('.' + classes.trigger).each(function(){

          const tooltip = $(this).data("tooltip");

          if (tooltip){
              const $tooltip = $("<div>").addClass("sub-domain-tabs__tooltip").text(tooltip);
              $(this).append($tooltip);
          }
        })
    }; // self.bindTabs

    self.switchTab = function(targetTab, $targetPanel) {
        // Add the 'on' state to the target tab and then
        // Remove the 'on' state from any other tabs
        targetTab.addClass(classes.onState)
                 .siblings().removeClass(classes.onState);

        // Add the 'on' state to the target panel and then
        // Remove the 'on' state from any other panels
        $targetPanel.addClass(classes.onState)
                    .siblings('.' + classes.target).removeClass(classes.onState);
    }; // self.switchTab

    self.initialize = function () {
        self.bindTabs();
    };

    return self;

})(jQuery);
