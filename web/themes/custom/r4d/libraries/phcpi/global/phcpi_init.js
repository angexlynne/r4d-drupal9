/// <reference path="/js/vendor/jquery/jquery-1.11.2.min.js"/>
/// <reference path="/js/vendor/jquery.collapsible.js"/>
/// <reference path="/js/phcpi/global/nav_drawer.js"/>
/// <reference path="/js/phcpi/global/tooltip.js"/>
/// <reference path="/js/phcpi/global/indicator_slider.js"/>
/// <reference path="/js/phcpi/global/collapsible.js"/>
/// <reference path="/js/phcpi/global/indicator_sticky.js"/>
/// <reference path="/js/phcpi/global/css_blendmode_check.js"/>
/// <reference path="/js/phcpi/global/tab_groups.js"/>
/// <reference path="/js/phcpi/global/mobile_legend.js"/>
/// <reference path="/js/phcpi/global/responsive-tables.custom.js"/>

'use strict';

var PHCPI = PHCPI || {};

(function ($, Drupal) {
  // Initialize various .js files here
  PHCPI.cssBlendModeCheck.initialize();
  PHCPI.navDrawer.initialize();
  PHCPI.tooltip.initialize();
  PHCPI.collapsible.initialize();
  PHCPI.indicatorSticky.initialize();
  PHCPI.indicatorSlider.initialize();
  PHCPI.tabGroups.initialize();
  PHCPI.mobileLegend.initialize();
})(jQuery, Drupal);
