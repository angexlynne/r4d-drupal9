﻿/// <reference path="/js/vendor/jquery/jquery-1.11.2.min.js"/>
/// <reference path="/js/vendor/angular/angular.js"/>
/// <reference path="/js/phcpi/shared/module.js" />

angular.module('phcpi.shared').directive('typeAhead', function() {
	return {
		restrict: 'A',
		scope: {
			selectCallback: "=selectcallback",
			data: "=data",
			group: "=group",
			disableInput: "=disableinput",
			disabledMessage: "=disabledmessage",
			groupSort: "=groupsort"
		},
		link: function(scope, element, attrs) {

			var clearOnSelected = attrs["clearonselected"] && attrs["clearonselected"].toLowerCase() === 'true';

			var substringMatcher = function(strs) {
				return function findMatches(q, cb) {
					var matches, substrRegex;

					// an array that will be populated with substring matches
					matches = [];

					// regex used to determine if a string contains the substring `q`
					substrRegex = new RegExp("\\b" + q, 'i');

					// iterate through the pool of strings and for any string that
					// contains the substring `q`, add it to the `matches` array
					for (var i = 0; i < strs.length; i++) {
						var str = strs[i];
						if (substrRegex.test(str.name)) {
							// the typeahead jQuery plugin expects suggestions to a
							// JavaScript object, refer to typeahead docs for more info
							matches.push(str);
						}
					}

					// Handle grouping
					if (scope.group) {
						var groupedMatches = angular.copy(matches).sort(function(a, b) {
							var aGroup = scope.group(a);
							var bGroup = scope.group(b);
							if (aGroup === bGroup) {
								return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
							}

							// If a custom sort function is set
							if(scope.groupSort){
								return scope.groupSort(a, b);
							}
							// Default alphabetical
							else{
								return aGroup > bGroup ? 1 : aGroup < bGroup ? -1 : 0;
							}
						});

						var lastGroup;

						groupedMatches.forEach(function(m) {
							if (!lastGroup || scope.group(m) !== lastGroup) {
								m.groupHeader = scope.group(m);
								lastGroup = scope.group(m);
							} else {
								m.groupHeader = '';
							}
						});

						cb(groupedMatches);
					} else {
						cb(matches);
					}
				};
			};

			var dataSource = scope.data;
			var $typeAheadBox = element.find('.typeahead');
			var currentSuggestion;

			function goToSelected() {

				// If there is a current suggestion from selecting from the list, or autocompleting
				if (currentSuggestion) {
					scope.selectCallback(currentSuggestion);

					if (clearOnSelected) {
						$typeAheadBox.val('');
						initTypeAhead();
					}

					return;
				}

				// If the user typed it all out instead
				var selectedValue = $typeAheadBox.typeahead('val');

				var itemMatch = dataSource.filter(function(c) {
					return c.name.toLowerCase() === selectedValue.toLowerCase();
				});

				if (itemMatch.length > 0) {
					var item = itemMatch[0];

					scope.selectCallback(item);

					if (clearOnSelected) {
						$typeAheadBox.val('');
						initTypeAhead();
					}
				}
			}

			function initTypeAhead() {
				// Destroy if it exists already
				$typeAheadBox.typeahead('destroy');

				// Unbind data handlers that aren't unbound in the 'destroy' method
				$typeAheadBox.unbind('typeahead:autocomplete');
				$typeAheadBox.unbind('typeahead:close');
				$typeAheadBox.unbind('typeahead:cursorchange');
				$typeAheadBox.unbind('typeahead:open');
				$typeAheadBox.unbind('typeahead:select');

				dataSource = scope.data;

				$typeAheadBox.typeahead({
					hint: false,
					highlight: true,
					minLength: 0
				}, {
					name: 'dataSource',
					displayKey: 'name',
					source: substringMatcher(dataSource),
					limit: 500,
					templates: {
						suggestion: Handlebars.compile('<div>{{#if groupHeader}}<div class="typeahead-group">{{groupHeader}}</div>{{/if}}<div class="typeahead-member">{{name}}</div></div>')
					}
				})
					.on('typeahead:select', function(e, suggestion, dataset) {
						scope.selectCallback(suggestion);

						if (clearOnSelected) {
							$typeAheadBox.val('');
							initTypeAhead();
						}

					})
					.on('typeahead:autocomplete', function(o, s, d) {
						currentSuggestion = s;
					})
					.on('typeahead:cursorchange', function(o, s, d) {
						currentSuggestion = s;
					})
					.on('typeahead:open', function() {
						// Clear out the current suggestion
						currentSuggestion = null;
					})
					.on('typeahead:close', function() {

						// Persist the current name
						if (currentSuggestion) {
							$typeAheadBox.val(currentSuggestion.name);
						}

						$typeAheadBox.val('');
					});
			}

			$typeAheadBox.keyup(function(e) {
				if (e.which == 13) {
					e.preventDefault();
					goToSelected();
				}
			});

			$typeAheadBox.siblings('button.drawer_content--button_submit').click(function(e) {
				e.preventDefault();
				goToSelected();
			});

			initTypeAhead();

			scope.$watch('data', function() {
				initTypeAhead();
			});

			var placeHolderText = $typeAheadBox.attr('placeholder');

			scope.$watch('disableInput', function() {

				if (scope.disableInput) {
					$typeAheadBox.attr('disabled', 'disabled');
					if(scope.disabledMessage){
						$typeAheadBox.attr('placeholder', scope.disabledMessage);
					}
					element.addClass('disabled');
				} else {
					$typeAheadBox.removeAttr('disabled');
					$typeAheadBox.attr('placeholder', placeHolderText);
					element.removeClass('disabled');
				}
			}, true);
		}
	};
});
