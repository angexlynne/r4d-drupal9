angular.module('phcpi.shared').directive('shareBar', ['$window', '$document', '$location', 'parameterUtil', 'appConfig', function ($window, $document, $location, parameterUtil, appConfig) {
  return {
    restrict: 'C',
    link: function (scope, element, attr) {
      $document.ready(function () {
        var newUrl = element.attr('data-print') == 'png' ? appConfig.webshot_url + '?url=http://' + $location.host() + element.attr('data-print-url') + window.encodeURIComponent('#') + window.encodeURIComponent($location.url()) + '&wait=true&ww=1200' : $location.absUrl() + '&wait=true';
        element.attr('href', newUrl);
      });

      scope.$on('$locationChangeSuccess', function() {
        var newUrl = element.attr('data-print') == 'png' ? appConfig.webshot_url + '?url=http://' + $location.host() + element.attr('data-print-url') + window.encodeURIComponent('#') + window.encodeURIComponent($location.url()) + '&wait=true&ww=1200' : $location.absUrl() + '&wait=true';
        element.attr('href', newUrl);
      });
    }
  }
}]);