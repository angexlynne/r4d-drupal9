angular.module('phcpi.shared').directive('pageShare', ['$window', '$document', '$location', 'parameterUtil', function ($window, $document, $location, parameterUtil) {
  return {
    restrict: 'C',
    link: function (scope, element, attr) {
      $document.ready(function () {
        $window.addthis.update('share', 'url', $location.absUrl());
      });

      scope.$on('$locationChangeSuccess', function() {
        $window.addthis.update('share', 'url', $location.absUrl());
      });
    }
  }
}]);