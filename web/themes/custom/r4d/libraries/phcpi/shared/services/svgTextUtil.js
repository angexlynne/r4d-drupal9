﻿/// <reference path="/js/vendor/jquery/jquery-1.11.2.min.js"/>
/// <reference path="/js/vendor/angular/angular.js"/>
/// <reference path="/js/phcpi/shared/module.js"/>

'use strict';

angular.module('phcpi.shared')
	.factory('svgTextUtil', function () {
		return {
			wrapText: function (text, width, wrapUp) {

				text.each(function () {
					var text = d3.select(this),
						words = text.text().split(/\s+/).reverse(),
						word,
						line = [],
						lineNumber = 0,
						lineHeight = 1.1, // ems
						y = text.attr("y"),
						x = text.attr("x"),
						dy = parseFloat(text.attr("dy")),
						tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy? dy + "em" : "");

					if(isNaN(dy)){
						dy = 0;
					}

					while (word = words.pop()) {
						line.push(word);
						tspan.text(line.join(" "));
						if (tspan.node().getComputedTextLength() > width) {
							line.pop();
							tspan.text(line.join(" ") + " ");
							line = [word];

							var calcDy = ++lineNumber * lineHeight + dy
							tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", calcDy? calcDy + "em" : "").text(word);
						}
					}

					if (wrapUp) {

						var allTspan = text.selectAll('tspan'),
							totalLines = allTspan[0].length,
							decrementEm = function (d, i) {
								var dy = +d3.select(this).attr('dy').replace('em', '');
								if(isNaN(dy)){
									dy = 0;
								}
								dy -= (totalLines - 1) * lineHeight;
								return dy? dy + "em" : "";
							};

						allTspan.attr('dy', decrementEm);
					}
				});
			}
		};
	});
