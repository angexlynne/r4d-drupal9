﻿/// <reference path="/js/vendor/jquery/jquery-1.11.2.min.js"/>
/// <reference path="/js/vendor/angular/angular.js"/>
/// <reference path="/js/phcpi/shared/module.js"/>

'use strict';

angular.module('phcpi.shared')
	.factory('parameterUtil', function () {
		return {

			// Parses a comma separated string of Ids
			parseIdString: function (idString) {

				if (!idString) {
					return [];
				}

				return idString.split(',').map(function (id) {
					return +id;
				});
			}

		};
	});