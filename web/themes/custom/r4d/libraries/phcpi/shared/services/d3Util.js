/// <reference path="/js/vendor/jquery/jquery-1.11.2.min.js"/>
/// <reference path="/js/vendor/angular/angular.js"/>
/// <reference path="/js/phcpi/shared/module.js"/>

'use strict';

angular.module('phcpi.shared')
	.factory('d3Util', function () {
		return {
			endAll: function(transition, callback) {
        if (transition.size() === 0) { callback() }
        var n = 0;
        transition
            .each(function() { ++n; })
            .each("end", function() { if (!--n) callback.apply(this, arguments); });
      }
		};
	});
