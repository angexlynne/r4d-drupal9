﻿/// <reference path="/js/vendor/jquery/jquery-1.11.2.min.js"/>
/// <reference path="/js/vendor/angular/angular.js"/>
/// <reference path="/js/phcpi/shared/module.js"/>

'use strict';

angular.module('phcpi.shared')
	.factory('vizTooltip', function () {
		return function (element) {

			var tooltipElement = angular.element('<div/>').attr('class', 'hover-tip');
			var contentContainer = angular.element('<div/>').attr('class', 'hover-content');

			tooltipElement.append(contentContainer);
			tooltipElement.append(angular.element('<div />').attr('class', 'arrow-down'));

			element.append(tooltipElement);

			return {
				show: function (x, y) {

					tooltipElement
						.css('left', (x - tooltipElement.outerWidth() / 2) + 'px')
						.css('top', (y - tooltipElement.outerHeight() - 5) + 'px')
						.show();

					// Reposition in case the tooltip was squished near the edges
					tooltipElement
						.css('left', (x - tooltipElement.outerWidth() / 2) + 'px')
						.css('top', (y - tooltipElement.outerHeight() - 5) + 'px');
				},

				hide: function () {
					tooltipElement.hide();
				},

				content: function (content) {
					contentContainer.empty();
					if (typeof content === 'object') {
						if (content.label) {
							contentContainer.append(angular.element('<span />').attr('class', 'value-label').text(content.label));
						}
						if (content.value) {
							contentContainer.append(angular.element('<span />').attr('class', 'value').text(content.value));
						}
						if (content.subValue) {
							contentContainer.append(angular.element('<span />').attr('class', 'sub-value').text(content.subValue));
						}
					} else {
						contentContainer.append(angular.element('<span />').attr('class', 'value').text(content));
					}
				}
			};
		};
	});