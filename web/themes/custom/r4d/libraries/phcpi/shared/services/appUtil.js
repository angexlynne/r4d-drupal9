/// <reference path="/js/vendor/jquery/jquery-1.11.2.min.js"/>
/// <reference path="/js/vendor/angular/angular.js"/>
/// <reference path="/js/phcpi/shared/module.js"/>

'use strict';

angular.module('phcpi.shared')
	.factory('appUtil', function () {

    function isEntitySelected(entity, selectedEntities) {
      if (!selectedEntities) {
        return false;
      }
      return selectedEntities.some(function (e) {
        return entity.id === e.id;
      });
    }

    function getEntitiesFromIds(entityIds, entities) {
      var entities = entities.filter(function (e) {
        return entityIds.indexOf(e.id) >= 0;
      });

      return entities;
    }

    return {

      selectEntities: function(newEntityIds, selectedEnties, allEntities, callback) {
        var newEntities = getEntitiesFromIds(newEntityIds, allEntities);
        var allSelectedEntities = selectedEnties || [];

        // Add newly selected entities, filtered to only ones that weren't previously selected
        allSelectedEntities = allSelectedEntities.concat(newEntities.filter(function (e) {
          return !isEntitySelected(e, selectedEnties);
        }));

        callback(allSelectedEntities);
      },

      setSelectableEntities: function(selectedEntities, availableEntities, callback) {
        callback(availableEntities.filter(function (e) {
          return !isEntitySelected(e, selectedEntities);
        }));
      },

      removeEntityFromSelected: function(entity, selectedEntities, callback) {
        callback(selectedEntities.reduce(function (accum, value) {
          if (value.id !== entity.id) {
            accum.push(value);
          }
          return accum;
        }, []));
      },

			sortByGroup: function(a, b){
				return +a.category.weight > +b.category.weight ? 1 : +a.category.weight < +b.category.weight ? -1 : 0;
			}

		};
	});
