'use strict';

angular.module('phcpi.shared')
	.factory('hoverEventHandler', function () {

		var func = function(container, hoverOpen, hoverClose){

			var touchTimeout;

			// Auto close the hover when it is opened by a touch
			// Close after 5 seconds
			function handleTouchHoverAutoClose(){
				if(touchTimeout){
					clearTimeout(touchTimeout);
				}
				touchTimeout = setTimeout(function(){
					hoverClose();
				}, 5000);
			}

			var moving = false;

			container.on('touchmove', function(){
				moving = true;
			});

			container.on('touchend', function(){
				if(!moving){
					d3.event.preventDefault();
					hoverOpen();
					handleTouchHoverAutoClose();
				}
				moving = false;
			});

			container.on('mousemove', function(){
				hoverOpen();
			})
			.on('mouseleave', function(){
				hoverClose();
			});

		};

		return func;

	});
