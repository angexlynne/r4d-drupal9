﻿/// <reference path="/js/vendor/jquery/jquery-1.11.2.min.js"/>
/// <reference path="/js/vendor/angular/angular.js"/>
/// <reference path="/js/phcpi/shared/module.js"/>

'use strict';

angular.module('phcpi.shared')
	.factory('colorGenerator', function () {

		var colors = [
			"#eeb704","#755fe3","#0968b6","#8fb901","#c22fa6","#574880","#427e2f","#dde4ff","#00a1dd","#7eb2ff","#fff1db",
			"#647f96","#005200","#3bc7ff","#323200","#00a578","#ff7592","#ede0d9","#bac694","#c02a3c","#fbc6ff","#ebad95",
			"#892970","#c85385","#5f1b00","#5567ea","#d08a00","#447069","#007b6a","#ef71ff","#ffe19c","#ffa000","#00fdb3",
			"#9491ff","#005bab","#00caab","#1e4940","#4c958a","#b1f8f7","#ffae00","#b12d02","#003c58","#8a8584","#a4af93",
			"#d8b700","#a13a1c","#88f889","#70495c","#3c3a46","#c7b273","#ff9742","#ecd06b","#ee5286","#344e49","#00b35a",
			"#bbaaae","#008d6f","#e7ffff","#cc8094","#813b33","#58626e","#927d72","#888289","#d2ccff","#afccec","#76ab73",
			"#ff84b6","#655652","#f3bf45","#5d652e","#008b7c","#007043","#c6d1d1","#6342ad","#00ffff","#965961","#e23a11",
			"#006f6b","#00357b","#647cb0","#0076fa","#ffb9af","#687400","#eee36b","#454a4d","#79767b","#5e2526","#958bb6",
			"#836661","#d5a405","#fa0087","#7ee1e7","#00a3ff","#005193","#634a00","#003d19","#c8ff8f","#ffb4ff","#cdad9c",
			"#009898","#a6c7ee","#5d5f78","#00687b","#19343f","#662d41","#cfeeeb","#e0d1ff","#96996c","#007200","#e897ff",
			"#ff8b00","#8c0071","#8f6fff","#fff25a","#f0edee","#f0e44e","#0052d9","#ff8d39","#18db5d","#e8f14c","#c764e6",
			"#f50078","#c06300","#8299c9","#0091d3","#4b581c","#00d8ff","#997769","#c26859","#98ffff","#ecf3e4","#880086",
			"#1b5246","#505050","#ff82ff","#ffdeff","#6bffff","#535650","#df5178","#9eca92","#0090ff","#00e794","#00fff5",
			"#ffb268","#77b2bc","#eb4be0","#00c780","#00344c","#aeb18e","#334b16",
		];

		return {
			generateColors: function(count) {
				if (count > 150) {
					throw "Can only support up to 150 colors at this time";
				}
				return colors.slice(0, count);
			},
			getColor: function(index) {
				if (index > 150) {
					throw "Can only support up to 150 colors at this time";
				}

				return colors[index];
			}
		};
	});