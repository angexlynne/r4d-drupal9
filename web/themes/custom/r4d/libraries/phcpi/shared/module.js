﻿/// <reference path="/js/vendor/jquery/jquery-1.11.2.min.js"/>
/// <reference path="/js/vendor/angular/angular.js"/>

'use strict';

angular.module('phcpi.shared', []).constant('appConfig', Drupal.settings.PHCPI.appConfig);