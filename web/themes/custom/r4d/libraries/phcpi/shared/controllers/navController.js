﻿/// <reference path="/js/vendor/jquery/jquery-1.11.2.min.js"/>
/// <reference path="/js/vendor/angular/angular.js"/>
/// <reference path="/js/phcpi/shared/module.js" />

angular.module('phcpi.shared').controller('NavController', ['$scope', 'appConfig', 'appUtil', '$location', '$anchorScroll', '$window',
  function ($scope, appConfig, appUtil, $location, $anchorScroll, $window) {
    $scope.navTypeaheadSelectionCallback = function(selection) {
  	window.location = selection.url;
    };

    $scope.countries = appConfig.countries;
    $scope.countryGroup = function(c){
  	return c.region;
    };

    $scope.indicators = appConfig.indicators;
    $scope.indicatorGroup = function(i){
  	return i.category.name;
    };

	$scope.categoryGroupSort = appUtil.sortByGroup;
	
    // Hack to let anchor links work with angular
    // This site doesn't have any location based angular routing, only search parameters
    // So if we see a location with no search, it is an anchor link
    //var first = true;
    $scope.$on('$locationChangeStart', function(e){
      if($location.path() && Object.keys($location.search()).length === 0){
        var hash = $location.path().replace('/', '');
        if(!hash){
          return;
        }
        $location.path('');
        $location.hash(hash);
        $anchorScroll();
      }
    });

}]);
